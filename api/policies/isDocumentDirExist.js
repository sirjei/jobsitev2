var fs = require('fs-extra');

module.exports = function (req, res, next) {
    var dir = sails.config.path;
    var publicDir = dir.public.uploadDocDir(req.user._id);
    var assetsDir = dir.assets.uploadDocDir(req.user._id);
    async.parallel({
        asset: function(callback){
            fs.ensureDir(assetsDir, function(err) {
                if(err) return callback(err);
                callback(null);
            });
        },
        public: function(callback){
            fs.ensureDir(publicDir, function(err) {
                if(err) return callback(err);
                callback(null);
            });
        }
    },
    function(err) {
        if(err) return res.serverError(err);
        next();
    });
};