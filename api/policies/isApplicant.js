module.exports = function(req, res, next) {
  //make sure sessionAuth policy has been initialized before this policy
  if (req.user.accountType == "applicant") return next();//check if user is applicant
      res.forbidden({error: 'custom', message:'You are not permitted to perform this action.'});
};