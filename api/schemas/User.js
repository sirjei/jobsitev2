

module.exports = function (mongoose) {

	return {
		attributes: {
				email: { 
					type: String, 
					unique: true,
					lowercase: true, 
					trim: true,
					validate: [ValidationService.email, ValidationService.max(254)],
					required: true
				},
				password: {
					type: String,
					trim: true,
					required: true,
					// validate: [ValidationService.max(50)] 
				},
				salt: {
					type: String,
					default: null
				},
				activationCode: {
					type: String,
					default: null
				},
				validated: {
					type: Boolean,
					default: false
				},
				active: {
					type: Boolean,	
					default: true
				},
				accountType: {
					type: String,
					enum: ['agency', 'applicant'],
					required: true
				},
				name: {
					type: String,
					trim: true,
					lowercase: true,
					required: true,
					validate: [ValidationService.max(256)]				
				},				
				applicant: { //id of the applicant
					type: mongoose.Schema.ObjectId,
					ref: 'Applicant',
				},
				///agency account additional details
				role: {
					type: String,
					enum: ["administrator", "moderator"],
				},
				agency: { //id of the company
					type: mongoose.Schema.ObjectId,
					ref: 'Agency',
				},				
		},

		methods: {
		//Here i defined my instances method for the model
	        validatePass : function (password) {
	            var hashPwd = EncryptionService.hashPwd(this.salt,password);  
	            if (hashPwd == this.password) {
	                return true;//password is correct
	            } else {
	                return false;
	            }
	        } 
		},

		statics : {
			// Here are the statics methods for the model 

		},

		beforeSave: function (values, next) {


		  	next();
		},
		middlewares: function (Schema) {

		},
		options: {
			strict: true
		}		
	};

}//end of exports
