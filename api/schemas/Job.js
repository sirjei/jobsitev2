

module.exports = function (mongoose) {

	return {
		attributes: {
			agency: { //id of the agency of the user who posted
				type: mongoose.Schema.ObjectId,
				ref: 'Agency',				
			},	
			postedBy: { //id of the user that posted
				type: mongoose.Schema.ObjectId,
				ref: 'User',				
			},							
			agencyName: {
	            type: String, 
	            lowercase: true,
	            required: true,
	            validate: [ValidationService.max(256)],
	            // index: 'text'					
			},	
			postedByName: {
	            type: String, 
	            lowercase: true,
	            required: true,
	            validate: [ValidationService.max(256)]					
			},				
			country: {
				type: String,
				lowercase: true,
				required: true,
			},	
			headline: {
	            type: String, 
	            lowercase: true,
	            required: true,
	            validate: [ValidationService.max(256)],
	            // index: 'text'				
			},
			keywords: [],					
			position: {
	            type: String,
	            lowercase: true,
	            required: true,
	            validate: [ValidationService.max(128)],
	            // index: 'text'
			},
			urgent: {
	            type: Boolean,
	            default: false
			},			
			datePosted: {
	            type: Date,
	            default: Date.now
	        },	        
	        // openUntil: {
	        //     type: Date,//expires in 5 mins
	           
	        //     required: true
	        // },
	        // vesselName: {
	        //     type: String, 
	        //     lowercase: true,
	        //     default: null,
	        //     validate: [ValidationService.max(128)]
	        // },	        
	        vesselType: {
	            type: String, 
	            lowercase: true,
	            default: null,
	            validate: [ValidationService.max(50)],
	            index: 'text'
	        },
	        proposedSalary:{
		        min: {
		            type: Number,
		            max: 9999999,
		            default: 0
		        },	        	
		        max: {
		            type: Number,
		            max: 9999999,
		            default: 0
		        },
		        currency: {
		            type: String,
		            default: 'usd'
		        },	        	
	        },
	        employmentBenefits: [],
	        qualifications: [],
	        trainings: [{
	        	_id: {
	        		type: Number,
	        		required: true,
	        	},
	        	name: {
		            type: String, 
		            required: true,
	        	}
	        }],
	        details: {
	            type: String, 
	            default: null,
	            validate: [ValidationService.max(512000)]
	        },	        
        	applications: [{//list of applicants
	        	applicant: {//id of the applicant
					type: mongoose.Schema.ObjectId,
					ref: 'User',
					required: true
	        	},
	        	rank: {
		            type: String, 
		            required: true,
		            lowercase: true,
		            validate: [ValidationService.max(128)]		        		
	        	},	        	 
	        	name: {
		            type: String, 
		            required: true,
		            lowercase: true,
		            validate: [ValidationService.max(128)]	        		
	        	},
		        dateCreated: {
		            type: Date,
		            default: Date.now		        	
		        }        		
        	}],
		},

		methods: {},
		statics : {},
		indexes: {
			
			position: 1,
			vesselType: 1,
			agencyName: 1,
			keywords: 1
		},
		beforeSave: function (values, next) {
			next();
		},
		middlewares: function (Schema) {},
		options: {
			strict: true,
			autoIndex: false
		}		
	};

}//end of exports
