
module.exports = function (mongoose) {

	return {
		attributes: {
			user: { 
				type: mongoose.Schema.ObjectId,
				ref: 'User' 
			},
			rank: {
				type: String,
				lowercase: true,
				trim: true,
				validate: [ValidationService.max(128)]
			},
			gender: {
				type: String,
				//enum: ['male', 'female'],
				default: null
			},			
			dob: {
				type: Date,
				default: null
			},
			nationality: {
				type: String,
				lowercase: true,
				trim: true,
				validate: [ValidationService.max(50)],
				default: null
			},						
			contactOne: {
				type: String,
				trim: true,
				validate: [ValidationService.max(21)],
				default: null			
			},
			contactTwo: {
				type: String,
				trim: true,
				validate: [ValidationService.max(21)],
				default: null
			},
	        expectedSalaryRange: {
		        min: {
		            type: Number,
		            max: 9999999,
		            default: 0
		        },	        	
		        max: {
		            type: Number,
		            max: 9999999,
		            default: 0
		        },
		        currency: {
		            type: String,
		            default: 'usd'
		        },
	        },			
			address: {
				type: String,
				lowercase: true,
				// required: true,
				trim: true,
				validate: [ValidationService.max(128)],
				default: null
			},
			city: {
				type: String,
				lowercase: true,
				// required: true,
				trim: true,
				validate: [ValidationService.max(128)],
				default: null
			},
			state: {
				type: String,
				lowercase: true,
				trim: true,
				// required: true,
				validate: [ValidationService.max(128)],
				default: null
			},
			country: {
				type: String,
				//required: true,
				trim: true,
				validate: [ValidationService.max(128)],
				default: null
			},
			zipCode: {
				type: String,
				// required: true,
				trim: true,
				validate: [ValidationService.max(11)],
				default: null
			},
			photo: {
				standard: {
					type: String,
					trim: true,
					default: null
				},
				thumb: {
					type: String,
					trim: true,					
					default: null
				}
			},
			documents: [{
				code: {
					type: String,
					unique: true,
					required:true
				},
				description: {
					type: String,
					trim: true,
					validate: [ValidationService.max(128)],
					required: true,					
				},
				number: {
					type: String,
					trim: true,		
					validate: [ValidationService.max(128)],			
					required: true
				},
				dateIssued: {
					type: Date,
					required: true
				},
				validUntil: {
					type: Date,
					//required: true
					default: null
				},
			}],
			trainings: [{
	        	_id: {
					type: Number,
	        	},				
				name: {
					type: String,
					trim: true,
					// validate: [ValidationService.max(128)],
					//required: true,
					default: null					
				},
			}],
			history: [{
				position: {
		            type: String, 
		            trim: true,
		            validate: [ValidationService.max(128)],
		            required: true
		        },
		        agency: {
		            type: String, 
		            trim: true,
		            validate: [ValidationService.max(128)],
		            required: true
		        },
		        salary: {
			        value: {
			            type: Number, 
			            max: 9999999,
			            default: 0
			        },
			        currency: {
			            type: String, 
			            default: 'usd'
			        }, 
		        },
       			signOn: {
		            type: Date, 
		            default: Date.now, 
		            required: true
		        },
		        signOff: {
		            type: Date, 
		            default: Date.now , 
		            required: true
		        },
		        vesselName: {
		            type: String, 
		            validate: [ValidationService.max(128)],
		            default: null
		        },		        
		        vesselType: {
		            type: String,
		            validate: [ValidationService.max(128)], 
		            default: null
		        },
		        route: {
		            type: String, 
		            validate: [ValidationService.max(128)],
		            default: null
		        },
		        engineType: {
		            type: String, 
		            validate: [ValidationService.max(128)],
		            default: null
		        },
		        flag: {
		            type: String, 
		            validate: [ValidationService.max(128)],
		            default: null
		        },
		        principal: {
		            type: String, 
		            validate: [ValidationService.max(128)],
		            default: null
		        },
		        masterNationality: {
		            type: String, 
		            validate: [ValidationService.max(128)],
		            default: null
		        },
		        grtkw: {
		            type: String, 
		            validate: [ValidationService.max(128)],
		            default: null
		        },		        
			}],
			uploads: [{
				description: {
					type: String,
					trim: true,
					validate: [ValidationService.max(128)],
					default: null				
				},
				filename: {
					type: String,
					trim: true,
					required: true					
				},
				basename: {
					type: String,
					trim: true,
					required: true					
				},
				extName: {
					type: String,
					trim: true,
					required: true					
				},								
				dateUploaded: {
					type: Date,
					default: Date.now
				},
				path: {
					type: String,
					trim: true,
					required: true					
				}
			}],			
			// followed: [{
			// 	//id of the agency being followed
			// 	agency: {
			// 		type: mongoose.Schema.ObjectId,
			// 		ref: 'Agency'
			// 	}
			// }],
		},

		methods: {
		//Here i defined my instances method for the model

		},

		statics : {
			// Here are the statics methods for the model 

		},
		beforeSave: function (values, next) {

	        next();
		},
		middlewares: function (Schema) {

		},		
		options: {
			strict: true
		}		
	};

}//end of exports
