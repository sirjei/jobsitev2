

module.exports = function (mongoose) {

	return {
		attributes: {
			jobId: {
				type: mongoose.Schema.ObjectId,
				unique: true,
				required: true,
			},
			headline:{
				type: String,
				required: true				
			},		
			agencyName: {
				type: String,
				required: true
			},
			bookmarkedBy: {
				type: mongoose.Schema.ObjectId,
				required: true,				
			}
		},

		methods: {},
		statics : {
			// Here are the statics methods for the model 

		},

		beforeSave: function (values, next) {


		  	next();
		},
		middlewares: function (Schema) {

		},
		options: {
			strict: true
		}		
	};

}//end of exports
