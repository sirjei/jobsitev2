

module.exports = function (mongoose) {

	return {
		attributes: {
			email: { 
				type: String, 
				unique: true,
				lowercase: true, 
				trim: true,
				required: true,
				validate: [ValidationService.email, ValidationService.max(128)]
			},
			password: {
				type: String,
				trim: true,
				required: true,
				validate: [ValidationService.max(50)] 
			},
			salt: {
				type: String,
				default: null
			},
			activationCode: {
				type: String,
				default: null
			},
			validated: {
				type: Boolean,
				default: false
			},
			active: {
				type: Boolean,	
				default: false
			},
			name: {
				first: {
					type: String,
					trim: true,
					lowercase: true,
					required: true,
					validate: [ValidationService.max(50)] 
				},
				last: {
					type: String,
					trim: true,
					lowercase: true,
					required: true,
					validate: [ValidationService.max(50)]
				},
			},
			role: {
				type: String,
				enum: ['superadmin','moderator'],
				trim: true,
				default: 'moderator', 
			},
			accountType: {
				type: String,
				enum: ['administrator'],
				default: 'administrator'
			},
		},

		methods: {
		//Here i defined my instances method for the model
			
			validatePass: function (password) {
			        var hashPwd = EncryptionService.hashPwd(this.salt,password);  
			        if (hashPwd == this.password) {
			            return true;//password is correct
			        } else {
			            return false;
			        }
			}
		},

		statics: {
			// Here are the statics methods for the model 
		},

		beforeSave: function (values, next) {
	        //create password before creation
	        var salt = EncryptionService.createSalt(),
	            hashPwd = EncryptionService.hashPwd(salt,values.password), 
	            hashActivation = EncryptionService.randomValueHex(12);
	        values.salt = salt;
	        values.password = hashPwd;
	        values.email = values.email.toLowerCase(); 
	        values.activationCode = hashActivation;

		  	next();
		},
		middlewares: function (Schema) {

		},
		options: {
			strict: true
		}		
	};

}//end of exports
