
module.exports = function (mongoose) {

	return {
		attributes: {
			name: {
				type: String,
				//unique: true,
				lowercase: true,
				trim: true,
				required: true,
				validate: [ValidationService.max(254)] 
			},
			contactOne: {
				type: String,
				trim: true,
				validate: [ValidationService.max(21)]				
			},
			contactTwo: {
				type: String,
				trim: true,
				validate: [ValidationService.max(21)]
			},	
			fax: {
				type: String,
				trim: true,
				validate: [ValidationService.max(21)]
			},
			websiteUrl: {
				type: String,
				trim: true,
				validate: [ValidationService.max(254)]
			},
			email: {
				type: String,
				trim: true,
				validate: [ValidationService.email, ValidationService.max(254)]
			},										
			address: {
				type: String,
				lowercase: true,
				// required: true,
				trim: true,
				validate: [ValidationService.max(128)]
			},
			city: {
				type: String,
				lowercase: true,
				// required: true,
				trim: true,
				validate: [ValidationService.max(128)]
			},
			state: {
				type: String,
				lowercase: true,
				trim: true,
				// required: true,
				validate: [ValidationService.max(128)]
			},
			country: {
				type: String,
				required: true,
				trim: true,
				validate: [ValidationService.max(128)]
			},
			zipCode: {
				type: String,
				// required: true,
				trim: true,
				validate: [ValidationService.max(11)]
			},
			photo: {
				standard: {
					type: String,
					trim: true,
					default: null
				},
				thumb: {
					type: String,
					trim: true,					
					default: null
				}
			},
			map: {
				longitude: {
					type: Number,
					default: 0
				},
				latitude: {
					type: Number,
					default: 0
				},
				zoom: {
					type: Number,
					default: 2
				}
			},
			socialMedia: [{
				code: {
					type: String,
					trim: true,
					validate: [ValidationService.max(128)],
					required: true
				},
				label: {
					type: String,
					trim: true,
					validate: [ValidationService.max(128)],
					required: true				
				},
				url: {
					type: String,
					trim: true,
					validate: [ValidationService.max(2083)],
				}
			}],
			documents: [{
				description: {
					type: String,
					lowercase: true,
					trim: true,
					validate: [ValidationService.max(128)],
					required: true					
				},
				number: {
					type: String,
					trim: true,		
					validate: [ValidationService.max(128)],			
					required: true	
				},
				dateIssued: {
					type: Date,
					required: true
				},
				validUntil: {
					type: Date,
					required: true
				}		
			}],
		
			// followers: [{
			// 	//id of the agency being followed
			// 	_id: {
			// 		type: mongoose.Schema.ObjectId,
			// 		ref: 'Applicant'
			// 	}
			// }],
			// ratings: [{
			// 	//id of the agency being followed
			// 	_id: {
			// 		type: mongoose.Schema.ObjectId,
			// 		ref: 'Applicant'
			// 	},
			// 	score: {
			// 		type: Number,
			// 		default: 0
			// 	}
			// }],
		},

		methods: {
		//Here i defined my instances method for the model

		},

		statics : {
			// Here are the statics methods for the model 

		},
		beforeSave: function (values, next) {

	        next();
		},
		middlewares: function (Schema) {

		},		
		options: {
			strict: true,
			collection: 'agencies'
		}		
	};

}//end of exports
