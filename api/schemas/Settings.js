

module.exports = function (mongoose) {

	return {
		attributes: {
			name: {
				type: String,
				trim: true,
				required: true,
			},
			slides: {
				items: [{
					path: {
						type: String,
						trim: true,
						required: true,
						default: null
					},
					order: {
						type: Number,
						trim: true,					
						default: 0
					}				
				}],
				speed: {
					type: Number,
					trim: true,					
					default: 0
				},
				transition: {
					type: String,
					trim: true,
					default: null					
				}
			},
				
			//list of training courses	
			trainings: [{
				description: {
					type: String,
					// lowercase: true,
					trim: true,
					validate: [ValidationService.max(128)],
					required: true
				}
			}],
			//list of job classifications
			jobs: [{
				description: {
					type: String,
					trim: true,
					// lowercase: true,
					validate: [ValidationService.max(128)],
					required: true
				},
				category: {
					type: String,
					trim: true,
					// lowercase: true,
					validate: [ValidationService.max(128)],
					required: true					
				},
			}],
			//list of default documents needed
			documents: [{
				description: {
					type: String,
					trim: true,
					// lowercase: true,
					validate: [ValidationService.max(128)],
					required: true
				},
				category: {//for applicant or agency
					type: String,
					trim: true,
					validate: [ValidationService.max(128)],
					required: true					
				},
				important: {
					type: Boolean,
					default: false
				}				
			}],
			//available currencies to display
			currencies: [{
				code: {
					type: String,
					trim: true,
					validate: [ValidationService.max(20)],
					required: true
				},
				name: {
					type: String,
					trim: true,
					validate: [ValidationService.max(20)],
					required: true					
				}
			}],
			vessels: [{
				description: {
					type: String,
					lowercase: true,
					trim: true,
					validate: [ValidationService.max(128)],
					required: true
				}
			}],			
			faq: [{
				// order: {
				// 	type: Number,
				// 	required: true
				// },				
				question: {
					type: String,
					trim: true,
					validate: [ValidationService.max(512000)],
					required: true
				},
				answer: {
					type: String,
					trim: true,
					validate: [ValidationService.max(512000)],
					required: true
				},
			}],			
		},

		methods: {},
		statics : {},
		beforeSave: function (values, next) {
		  	next();
		},
		middlewares: function (Schema) {},
		options: {
			strict: true,
			collection: 'settings'
		}		
	};

}//end of exports
