var validator = require('validator');
module.exports = {
	email: { 
		validator: function (value) {
			return validator.isEmail(value); 
		}, 
		msg: '{PATH} field is not a valid email}' 
	},
	max: function (max) {
		return {
			validator: function (value) {
				return validator.isLength(value, 0, max);
			},
			msg: '{PATH} field exceeded the maximum length of '+max
		}		
	},
	min: function (min) {
		return {
			validator: function (value) {
				return validator.isLength(value, min)
			},
			msg: '{PATH} field exceeded the minimum length of '+min
		}
	},
	isLowerCase: function (value, onError) {
		
		if (!validator.isLowercase(value)) {
			onError('not lowercase','test',value);
		}else{
			return true;
		}
	}
	
};