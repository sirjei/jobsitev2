module.exports = {
	overwrite: function (source, destination) {

		for (var prop in source) {
		  if(_.has(source, prop)){//make sure not inherited

		  	if (_.isArray(source[prop])) {//if array
		  		for (var i = 0; i < source[prop].length; i++) {
		  			this.overwrite(source[prop][i], destination[prop][i]);
		  		};
		  	} else if (_.isObject(source[prop])) {//if object
		  		this.overwrite(source[prop], destination[prop]);
		  	} else {
		  		destination[prop] = source[prop];
		  	}
	      	
	      }
	    }
	},

};