var crypto = require('crypto');


module.exports = {
	sha1: function (text) {
	    return crypto.createHash('sha1').update(text).digest('hex');
	},
	createSalt: function (){
		return crypto.randomBytes(128).toString('base64');
	},
	hashPwd: function (salt,pwd){
		var hmac = crypto.createHmac('sha1',salt);
		return hmac.update(pwd).digest('hex');
	},
    randomValueHex : function (len){
        return crypto.randomBytes(Math.ceil(len/2))
            .toString('hex') // convert to hexadecimal format
            .slice(0,len);   // return required number of characters        
    }
};
