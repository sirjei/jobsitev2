var mongoose = require('mongoose');
var glob = require("glob");
var path = require('path');
var models = {};
/**
 
* Let's make our Mongodb Schemas/Models
 
*/
 
function iterateModels() {
sails.log.info('Preparing Mongoose Models');
	glob("api/schemas/*.js", {}, function (err, files){
		//console.log(files);
		for (var i = files.length - 1; i >= 0; i--) {
			var basename = path.basename(files[i], '.js');

			sails.log.info(process.cwd()+ '/' + files[i]);
			var tmp_schema = require(process.cwd()+ '/' + files[i])(mongoose);
			models[basename] = createMongooseModel(tmp_schema, basename);

			if (!_.isEmpty(tmp_schema.indexes)) {
				//set indexes
				models[basename].collection.ensureIndex(tmp_schema.indexes, function(error, res) {
			    if(error){
			        return console.error('failed ensureIndex with error', error);
			    }
			    console.log('ensureIndex succeeded with response', res);
				});  				
			}
		};
	});
};
 
function createMongooseModel(schema_description, model_name) {
	 var schema = new mongoose.Schema(schema_description.attributes,schema_description.options);
	 // if(!_.isEmpty(schema_description.indexes)) {
	 // 	sails.log.info("indexes:",schema_description.indexes);
	 // 	schema.index(schema_description.indexes);
	 // }
	 if (schema_description.methods)
	 	schema.methods = schema_description.methods;
	 if (schema_description.statics)
	 	schema.statics = schema_description.statics;
	 if (schema_description.beforeSave)
	 	schema.pre('save', function (next){
	 		schema_description.beforeSave(this, next);
	 	});
	 if (schema_description.middleware)
	 	    schema_description.middleware(schema);

	return mongoose.model(model_name, schema);
};

iterateModels(); 
// Expose Mixed type and ObjectId type for Models
models.Mixed = mongoose.Schema.Types.Mixed;
models.ObjectId = mongoose.Schema.Types.ObjectId;
// Expose all models loaded
module.exports = models;