/**
 * UploadsController
 *
 * @description :: Server-side logic for managing uploads
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var path = require('path'),
	gm = require('gm'),
    fs = require('fs-extra');
module.exports = {
	receive: function (req, res) {
		var userID = req.user._id;
        var dir = sails.config.path;
        //start uploading
        
        req.file('documents').upload({
            dirname: dir.temp.uploads,
        }, function (err, uploadedFiles) {
          if (err) return res.serverError(err);

          		//validate first
	            var fileType = '|' + uploadedFiles[0].type.slice(uploadedFiles[0].type.lastIndexOf('/') + 1) + '|';
	            var ifTypeValid = '|pdf|'.indexOf(fileType) !== -1;

				if (uploadedFiles[0].size > 2000000) {
					//show error
					res.badRequest({error:'custom', message: 'File size is greater than 2mb'});
				} else if (!ifTypeValid) {
					//show error
					res.badRequest({error:'custom', message: 'Only pdf files allowed'});
				} else {
                //the dir where the file has been uploaded
                var originalFile = uploadedFiles[0].fd;
                var originalFileName = uploadedFiles[0].filename;
                //directory where the document will be moved
                var publicCopy = dir.public.uploadDocDir(userID,originalFileName);

                var counter = 1;
                async.forever(
                    function(next) {
                        fs.exists(publicCopy, function(err) {
                          if (err) {//if error then it exists
                            //rename the file again
                            var extName = path.extname(originalFileName);
                            var baseName = path.basename(originalFileName, extName);
                            var newName = baseName+'('+counter+')'+extName;
                            counter++;
                            publicCopy = dir.public.uploadDocDir(userID,newName);
                            next();
                          }else{
                          	next(true);
                          }
                        });
                    },
                    function(err) {
                        //the file does not exist start moving to public folder

                        fs.move(originalFile,publicCopy, function(err){
                          	if (err) return res.serverError(err);
                            //sails.log.info("success uploaded a file");
                            var extName = path.extname(publicCopy);
                            var baseName = path.basename(publicCopy, extName);                            
                            var filename = baseName+extName;
                            var assetCopy = dir.assets.uploadDocDir(userID,filename);
                            //then create a copy to the asset folder
                            fs.copy(publicCopy,assetCopy, function(err){
                              if (err) { console.error(err); }
                            }); //copies file                            
                            
                            db.Applicant.findOne({user: userID}).exec(function(err,result){
                                if (err) return res.serverError(err);
                                //res.ok(created);
                                if (result) {


                                	var data = {
										filename: filename,
										basename: baseName,
										extName: extName,
										path: 'uploads/'+userID+'/doc/'+filename                             		
                                	};
                                	result.uploads.push(data);
                                	result.save(function (err) {
                                		if (err) return res.serverError(err);
                                		res.ok();
                                	});
                                } else {
                                	res.serverError({error:'custom', message: 'Could not identify who uploaded the documents'});
                                }
                            });
                          
                        }); 
                 });//end of forever 
				}//end of validation
           
            
        });//end of uploader		
	},	
	getAll: function (req, res) {
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				res.ok(result.uploads);
			} else {
				res.forbidden();
			}
		});	
	},
	updateOne: function (req, res) {
		//init
		var id = req.param('id');
		var payload = req.body;
		var userID = req.user._id;	
		var dir = sails.config.path;	

		function step1 ()	 {

			db.Applicant.findOne({user: userID}).exec(function (err, result) {
				if (err) return res.serverError(err);
				if (result) {
					//result.uploads = req.body.documents;
					var upload = result.uploads.id(id);
					if (upload) {//if id exists
						var index = result.uploads.indexOf(upload);
						var item = result.uploads[index];
						
						if (item.basename !== payload.basename) {//if its not the same name
							step2(result, index);
						} else {
							//update the description only
							result.uploads[index].description = payload.description;
							result.save(function (err) {
								if (err) return res.validatorError(err); 
								res.ok(item);
							})
							
						}
				
					} else {
						res.ok(upload);
					}

				} else {
					res.forbidden();
				}
			});

		};//end of step 1

		function step2 (result, index) {//start renaming
			//start on assets folder first
			var filename = payload.basename+payload.extName;//get the modified filename
			var basename = payload.basename;
			var checkFile = dir.assets.uploadDocDir(userID,filename);
			var counter = 1;
			async.forever(function(next) {
				//check if the file exists
                fs.exists(checkFile, function(err) {
                  if (err) {//if error then it exists
                    //rename the file again
                    filename = payload.basename+'('+counter+')'+payload.extName;
                    basename = payload.basename+'('+counter+')';
                    counter++;
                    checkFile = dir.assets.uploadDocDir(userID,filename);
                    next();
                  }else{
                  	//does not exist use the lastest filename
                    next(true);
                  }
                });				
            },function(err) {
            	var data = {
            		filename: filename,
            		basename: basename,
            	};
            	
				step3(data, result, index);
            });
		};

		function step3 (data, result, index) {
			//save first before renaming
			var oldFilename = result.uploads[index].filename;

			result.uploads[index].basename = data.basename;
			result.uploads[index].filename = data.filename;
			result.uploads[index].description = payload.description;
			
			result.save(function (err) {
				if (err) return res.validatorError(err);
				async.parallel({
				    publicCopy: function(callback){

				    	var oldFile = dir.public.uploadDocDir(userID,oldFilename);
				    	var newFile = dir.public.uploadDocDir(userID,data.filename);
		 				fs.rename(oldFile, newFile, function (err) {
		                    callback(err);
		                });	
				    },
				    assetCopy: function(callback){
				    	
				    	var oldFile = dir.assets.uploadDocDir(userID,oldFilename);
				    	var newFile = dir.assets.uploadDocDir(userID,data.filename);
		 				fs.rename(oldFile, newFile, function (err) {
		                    callback(err);
		                });
				    }
				},
				function(err) {
					//start saving
					if (err) return res.serverError(err)
					res.ok(result.uploads[index]);
				});					
			});


		
		};

		step1();
	
	},
	deleteOne: function (req, res) {
		var id = req.param('id');
		var userID = req.user._id;

		db.Applicant.findOne({user: userID}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {

				//find the document to be deleted
		        var dir = sails.config.path;

				var upload = result.uploads.id(id);
				if (upload) {//if found delete the file

					var publicCopy = dir.public.uploadDocDir(userID,upload.filename);
	                var assetCopy = dir.assets.uploadDocDir(userID,upload.filename);
	                async.parallel([
	                    function(callback){
	                        fs.remove(publicCopy, function(err){
	                          if (err) { sails.log.error('file removed:',err); }
	                          callback();
	                        });
	                    },
	                    function(callback){
	                        fs.remove(assetCopy, function(err){
	                          if (err) { sails.log.error('file removed:',err); }
	                          callback();
	                        });
	                    }
	                ],
	                function(err){

	                    upload.remove();
			            // apply the delete   
						result.save(function (err) {
							if (err) return res.validatorError(err);
							res.ok(upload);
						});	                    
	                });

				} else {
					res.ok(upload);
				}


			} else {
				res.forbidden();
			}
		});	
	},
	photo: function (req, res) {

	        var dir = sails.config.path;
	        var userID = req.user._id;
	        var userAccountType = req.user.accountType;

	        req.file('photo')
	        .upload({
	            dirname:dir.temp.uploads
	        },function whenDone(err, uploadedFiles) { 
	            if (err) return res.negotiate(err);   
	                //global
	                var ext = path.extname(uploadedFiles[0].fd);
	                var basename = path.basename(uploadedFiles[0].fd,ext);
	                var originalFile = uploadedFiles[0].fd;
	                var filename = path.basename(uploadedFiles[0].fd);
	                var thumb = basename+'_thumb'+ext;

	            var step1 = function (callback) {//check file size
	                
	                if(uploadedFiles[0].size > 2000000) {//if greater than 2mb
	                    fs.remove(originalFile, function(err){
	                        if (err) {
	                            sails.log.error('Remove Error:',err);
	                        }
	                        callback({error:'custom',message:'File size greater than 2mb'});
	                    });                 
	                } else {
	                    callback(null);
	                }
	            };

	            var step2 = function(callback){//convert and copy to their respective folders after upload
	                
	                //make sure both are copied in the private and temp
	                var publicCopyThumb = dir.public.uploadPhotoDir(userID,thumb);
	                var publicCopy = dir.public.uploadPhotoDir(userID,filename);
	                var assetsCopyThumb = dir.assets.uploadPhotoDir(userID,thumb);                     
	                var assetsCopy = dir.assets.uploadPhotoDir(userID,filename); 
	                
	                var imageMagick = gm.subClass({imageMagick: true});
	                var settings = imageMagick(originalFile);
	                settings.autoOrient();
	                settings.setFormat('jpg');
	                
	                async.parallel({//after every conversion make sure copy it to the asset folder
	                    convert150x150: function(callback){
	                        settings.resize(150, 150, "!")
	                        settings.write(publicCopy, function (err) {
	                            if (err) return callback(err);  
	                            fs.copy(publicCopy,assetsCopy, function(err){
	                                if (err) return callback(err);
	                                callback(null); 
	                            });                        
	                        });
	                    },                    
	                    convert35x35: function(callback){
	                        settings.resize(35, 35, "!")
	                        settings.write(publicCopyThumb, function (err) {
	                            if (err) return callback(err);  
	                            fs.copy(publicCopyThumb,assetsCopyThumb, function(err){
	                                if (err) return callback(err);
	                                callback(null); 
	                            });                        
	                        });
	                    },

	                },
	                function(err) {
	                    if (err) return callback(err);
	                    //after convert and copy delete the source
	                    fs.remove(originalFile, function(err){
	                        if (err) {
	                            sails.log.error('Remove Error:',err);
	                        }
	                        callback(null);
	                    }); 
	               
	                });                 
	            };//end of step2 

	            var step3 = function (callback){
	                //get the photo id depending on the user type
	                switch (userAccountType) {
	                        case 'applicant':
	                                db.Applicant.findOne({user: userID}).exec(function(err,applicant){
	                                    if (err) return callback(err);
	                                    if (applicant) {
	                                        callback(null,applicant);
	                                    }else{
	                                        callback({error: 'custom',message:'User not found'});
	                                    }
	                                });          
	                            break;
	                        case 'agency':
	                                db.Agency.findOne({user: userID}).exec(function(err,company){
	                                    if (err) return callback(err);
	                                    if (company) {
	                                        callback(null,company);
	                                    }else{
	                                        callback({error: 'custom',message:'User not found'});
	                                    }                                            
	                                });                    
	                            break;            
	                }                
	            };//end of step 3

	            var step4 = function (user,callback){
	                //delete the old pictures
	                if ((user.photo.standard !== null) && (user.photo.thumb !== null)){
	                    var standardPhoto = path.basename(user.photo.standard);
	                    var thumbPhoto = path.basename(user.photo.thumb);

	                    //make sure both are deleted in the private and temp
	                    var publicCopyThumb = dir.public.uploadPhotoDir(userID,thumbPhoto);
	                    var assetsCopyThumb = dir.assets.uploadPhotoDir(userID,thumbPhoto);                     
	                    var publicCopy = dir.public.uploadPhotoDir(userID,standardPhoto);
	                    var assetsCopy = dir.assets.uploadPhotoDir(userID,standardPhoto);  

	                    var deleteFiles = [publicCopyThumb,publicCopy,assetsCopyThumb,assetsCopy];
	     
	                    async.each(deleteFiles, function(file, callback) {
	                        fs.remove(file, function(err){
	                            //if (err) return callback(err);
	                            callback(null);
	                        });                
	                    },function(err){
	                        // if (err) {
	                        //     sails.log.error('Remove Error:',err);
	                        // }
	                        callback(null,user);
	                    });                                        
	                }else{
	                    callback(null,user);
	                }
	                


	                
	            };//end of step4


	            async.waterfall([step1,step2,step3,step4],function(err, user){
	                if (err) return res.serverError(err);
	                
	                user.photo.standard = 'uploads/'+userID+'/photo/'+basename+'.jpg';
	                user.photo.thumb = 'uploads/'+userID+'/photo/'+basename+'_thumb.jpg';
	                user.save(function(err){
	                    if (err) return res.serverError(err);
	                    if (err) {
	                        if (err.error && err.error == 'custom') {
	                            res.badRequest(err);
	                        } else {
	                            res.serverError(err);
	                        }
	                    } else {
	                        res.ok(user.photo);
	                    }
	      
	                    //sails.sockets.blast('photo', {id:userId,photo:path,filename:basename,photoType:req.user.type});
	                    
	                });
	            });//end of waterfall 

	        });//.end of upload
	}		
};

