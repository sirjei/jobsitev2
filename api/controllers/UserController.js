/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require('passport'),
    path = require('path'),
    fs = require('fs-extra');
module.exports = {
	login: function (req, res) {
        var auth = passport.authenticate('default',function(err,user,status){
            if(err){
                res.serverError(err);
            } else {
                if(!user){
                    if (err) return res.serverError(err);  
                    if (status) return res.badRequest(status);
                    
                }else{
                    //sails.log.info('USER',user);
                    db.User.findOne({_id: user._id})
                    .exec(function(err,user){
                        if(err){
                            res.serverError(err);
                        }else{
                            req.logIn(user,function(err){
                               if(err){
                                res.serverError(err);
                               } else {

                                    switch (user.accountType) {
                                      case 'applicant':
                                        db.User.findOne({_id: user._id},'accountType applicant').populate('applicant', '_id name photo').exec(function(err,user){
                                           if (err) return res.serverError(err)
                                            res.ok(user);
                                        });
                                        break;
                                      case 'agency':
                                        db.User.findOne({_id: user._id},'accountType agency').populate('agency', '_id map name photo').exec(function(err,user){
                                           if (err) return res.serverError(err)
                                            res.ok(user);
                                        });      
                                        break;        
                                    }   
                                                                 
                               }
                            });                            
                        }
                    }); 
                }            
            }
        });
        auth(req,res);
    },
    logout: function (req, res){
    
        req.logout();
        res.ok();
    },	
    status: function (req, res) {
        if (req.isAuthenticated()) {
            res.ok(req.user);
        } else {
            res.loginRequired();
        }
    },
    ensureEmail: function (req, res) {
        db.User.findOne({email: req.body.email}).exec(function (err, found) {   
            if (err) return res.negotiate(err);
            if (!found) return res.ok({emailExists: false});
            res.ok({emailExists: true});
        });
    },
    ensureAgencyName: function (req, res) {
        db.Agency.findOne({name: req.body.name}).exec(function (err, found) {   
            if (err) return res.negotiate(err);
            if (!found) return res.ok({nameExists: false});
            res.ok({nameExists: true});
        });
    },
    info: function (req, res) {

        db.User.findOne({_id: req.user._id}, 'email '+req.user.accountType).populate(req.user.accountType)
        .exec(function (err, user) {
            if (err) return res.serverError(err);
            res.ok(user);
        });
    },
    geo: function (req, res) {
        
    }
};

