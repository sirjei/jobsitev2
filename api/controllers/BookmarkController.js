/**
 * BookmarkController
 *
 * @description :: Server-side logic for managing bookmarks
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	addJobs: function (req, res) {
		var payload = req.body;
		if (req.isAuthenticated()) { // check if user is authenticated

			switch (req.user.accountType) {
				case "applicant":
						//expects array of objects

						async.map(payload.bookmarks, function (item, callback) {

							var data = {
								jobId: item.jobId,
								headline: item.headline,		
								agencyName: item.agencyName,
								bookmarkedBy: req.user._id
							};	

							db.BookmarkJob.findOne(data).exec(function (err, result) {
								if (err) return callback(null,{status: "error",data: result,errType:"serverError", errMsg: err});
								if (result) {
									callback(null,{status: "error",data: result,errType:"badRequest", errMsg: "already on your bookmark list"});
								} else {
									db.BookmarkJob.create(data).exec(function (err, result) {
										if (err) return callback(null,{status: "error",data: result,errType:"negotiate", errMsg: err});
										callback(null,{status: "success",data: result});
									})
								}
							});

						}, function(err, results){
						    // return a transformed array of success and failed insert
						    res.ok(results);
						});

					break;
				case "agency":
						res.forbidden({error: "custom",msg: "Only applicants can bookmark a job post"});
					break;
				default:
						res.notFound({});
					break;										
			}

		} else {
			res.loginRequired({msg: "unauthenticated user, use cookies for bookmarks"});
		}

	},	
	addOneJob: function (req, res) {
		var payload = req.body;
		if (req.isAuthenticated()) { // check if user is authenticated

			switch (req.user.accountType) {
				case "applicant":
						//expects an object
						var data = {
							jobId: payload.jobId,
							headline: payload.headline,		
							agencyName: payload.agencyName,
							bookmarkedBy: req.user._id
						};						
			
						//check if the bookmark is already on the list
						db.BookmarkJob.findOne(data).exec(function (err, result) {
							if (err) return res.serverError(err);
							if (result) {
								res.badRequest({error: "custom",msg: "already on your bookmark list"});
							} else {
								
								db.BookmarkJob.create(data).exec(function (err, result) {
									if (err) return res.negotiate(err);
									res.ok(result);
								})
							}
						});

					break;
				case "agency":
						res.forbidden({error: "custom",msg: "Only applicants can bookmark a job post"});
					break;
				default:
						res.notFound({});
					break;										
			}

		} else {
			res.loginRequired({msg: "unauthenticated user, use cookies for bookmarks"});
		}

	},	
	removeOneJob: function (req, res) {
		//check if the bookmark is already on the list
		var id = req.query.id;
		if (req.isAuthenticated()) { // check if user is authenticated

			switch (req.user.accountType) {
				case "applicant":
						//expects an object
						db.BookmarkJob.findOneAndRemove({_id:id}).exec(function (err, result) {
							if (err) return res.serverError(err);
							if (result) {
								res.ok(result);
							} else {
								res.notFound({error:"custom",msg: "does not exist"});
							}
						});

					break;
				case "agency":
						res.forbidden({error: "custom",msg: "Only applicants can bookmark a job post"});
					break;
				default:
						res.notFound({});
					break;										
			}

		} else {
			res.loginRequired({msg: "unauthenticated user, use cookies for bookmarks"});
		}		
	},
	getAllJobs: function (req, res) {
		if (req.isAuthenticated()) { // check if user is authenticated
			db.BookmarkJob.find({bookmarkedBy: req.user._id}).exec(function (err, results) {
				if (err) return res.serverError(err);
				res.ok(results);
			});
		} else {
			res.loginRequired({msg: "unauthenticated user, use cookies for bookmarks"});
		}	
	}
};

