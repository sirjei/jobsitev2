/**
 * SettingsController
 *
 * @description :: Server-side logic for managing settings
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	//get training list
	getTrainingList: function (req, res) {
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
			 	if (err) return res.validatorError(err);
				if(found){
					//sails.log.info(found);
					res.ok(found.trainings)

				}else{
					res.serverError();
				}
			});
	},	
	createTrainingList: function (req, res) {

			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
			 	if (err) return res.validatorError(err);
				if(found){
					sails.log.info(found);
					found.trainings.push({description: req.body.description});
					found.save(function (err, saved) {
						sails.log.info('debug:',err);
							if (err) return res.validatorError(err);
							if (saved) {
								var inserted = _.last(saved.trainings);
								res.ok(inserted);								
							} else {
								res.serverError();
							}
					});
				}else{
					db.Settings.create({name: 'settings_v1'}, function (err, created) {
						if (err) return res.validatorError(err);
						created.trainings.push({description: req.body.description});
						created.save(function (err, saved) {
								if (err) return res.validatorError(err);
								if (saved) {
									var inserted = _.last(saved.trainings);
									res.ok(inserted);								
								} else {
									res.serverError();
								}
						});						
					})
				}
			});
	},
	updateTrainingList: function (req, res) {
		var id = req.param('id');
		
		if (id)	{
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
				if (err) return res.negotiate(err);
				if (settings) {
					for (var i = 0; i < settings.trainings.length; i++) {
						
						if (settings.trainings[i]._id == id) {
						   CollectionService.overwrite(req.body, settings.trainings[i]);
						}
					}
					settings.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok();
					});
				} else {
					res.serverError({name: 'custom', type: 'danger', message: "Settings does not exist"});
				}
			});			
		} else {
			res.notFound();
		}

	},
	destroyTrainingList: function (req, res) {
			var id = req.param('id');
			
			if (id)	{
				db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
					if (err) return res.negotiate(err);
					if (settings) {

						var trainings = settings.trainings.id(id).remove();
						sails.log.info(trainings);

						settings.save(function (err) {
							if (err) return res.validatorError(err);
							res.ok(trainings);
						});
					} else {
						res.serverError({name: 'custom', type:'danger', message: "Settings does not exist"});
					}
				});			
			} else {
				res.notFound();
			}
	},
	//document
	getDocumentList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				//sails.log.info(found);
				res.ok(found.documents)
			}else{
				res.serverError();
			}
		});
	},	
	createDocumentList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				sails.log.info('FOUND', found)
				found.documents.push(req.body);
				found.save(function (err, saved) {
					sails.log.info('debug:',err);
						if (err) return res.validatorError(err);
							if (saved) {
								var inserted = _.last(saved.documents);
								res.ok(inserted);								
							} else {
								res.serverError();
							}
				});
			}else{
				sails.log.info('NOT FOUND', found)
				db.Settings.create({name: 'settings_v1'}, function (err, created) {
					if (err) return res.validatorError(err);
					created.documents.push(req.body);
					created.save(function (err, saved) {
							if (err) return res.validatorError(err);
							if (saved) {
								var inserted = _.last(saved.documents);
								res.ok(inserted);								
							} else {
								res.serverError();
							}
					});						
				})
			}
		});
	},
	updateDocumentList: function (req, res) {
		var id = req.param('id');
		
		if (id)	{
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
				if (err) return res.negotiate(err);
				if (settings) {
					for (var i = 0; i < settings.documents.length; i++) {
						
						if (settings.documents[i]._id == id) {
							for (var prop in req.body) {

						      if(req.body.hasOwnProperty(prop)){
						      	settings.documents[i][prop] = req.body[prop]  
						      }
						   }
						   break;
						}
					}
					settings.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok();
					});
				} else {
					res.serverError({name: 'custom', type: 'danger', message: "Settings does not exist"});
				}
			});			
		} else {
			res.notFound();
		}

	},
	destroyDocumentList: function (req, res) {
			var id = req.param('id');
			
			if (id)	{
				db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
					if (err) return res.negotiate(err);
					if (settings) {

						var documents = settings.documents.id(id);
						if(documents) {
							documents.remove();
							settings.save(function (err) {
								if (err) return res.validatorError(err);
								res.ok(documents);
							});
						} else {
							res.notFound();
						}
					} else {
						res.serverError({name: 'custom', type: 'danger', message: "Settings does not exist"});
					}
				});			
			} else {
				res.notFound();
			}
	},
	//document
	getJobList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				//sails.log.info(found);
				res.ok(found.jobs)
			}else{
				res.serverError();
			}
		});
	},	
	createJobList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				
				found.jobs.push(req.body);
				found.save(function (err, saved) {
					//sails.log.info('debug:',err);
						if (err) return res.validatorError(err);
						if (saved) {
							var inserted = _.last(saved.jobs);
							res.ok(inserted);								
						} else {
							res.serverError();
						}
				});
			}else{
				
				db.Settings.create({name: 'settings_v1'}, function (err, created) {
					if (err) return res.validatorError(err);
					created.jobs.push(req.body);
					created.save(function (err, saved) {
							if (err) return res.validatorError(err);
							if (saved) {
								var inserted = _.last(saved.jobs);
								res.ok(inserted);								
							} else {
								res.serverError();
							}

					});						
				})
			}
		});
	},
	updateJobList: function (req, res) {
		var id = req.param('id');
		
		if (id)	{
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
				if (err) return res.negotiate(err);
				if (settings) {
					for (var i = 0; i < settings.jobs.length; i++) {
						
						if (settings.jobs[i]._id == id) {
							for (var prop in req.body) {

						      if(req.body.hasOwnProperty(prop)){
						      	settings.jobs[i][prop] = req.body[prop]  
						      }
						   }
						   break;
						}
					}
					settings.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok();
					});
				} else {
					res.serverError({name: 'custom', type: 'danger',message: "Settings does not exist"});
				}
			});			
		} else {
			res.notFound();
		}

	},
	destroyJobList: function (req, res) {
			var id = req.param('id');
			
			if (id)	{
				db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
					if (err) return res.negotiate(err);
					if (settings) {

						var job = settings.jobs.id(id);
						if(job) {
							job.remove();
							settings.save(function (err) {
								if (err) return res.validatorError(err);
								res.ok(job);
							});
						} else {
							res.notFound();
						}
					} else {
						res.serverError({name: 'custom', type: 'danger', message: "Settings does not exist"});
					}
				});			
			} else {
				res.notFound();
			}
	},
	createCurrencyList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				
				found.currencies.push(req.body);
				found.save(function (err, saved) {
					//sails.log.info('debug:',err);
						if (err) return res.validatorError(err);
						if (saved) {
							var inserted = _.last(saved.currencies);
							res.ok(inserted);								
						} else {
							res.serverError();
						}
				});
			}else{
				
				db.Settings.create({name: 'settings_v1'}, function (err, created) {
					if (err) return res.validatorError(err);
					created.currencies.push(req.body);
					created.save(function (err, saved) {
							if (err) return res.validatorError(err);
							if (saved) {
								var inserted = _.last(saved.currencies);
								res.ok(inserted);								
							} else {
								res.serverError();
							}

					});						
				})
			}
		});
	},
	updateCurrencyList: function (req, res) {
		var id = req.param('id');
		
		if (id)	{
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
				if (err) return res.negotiate(err);
				if (settings) {
					for (var i = 0; i < settings.currencies.length; i++) {
						
						if (settings.currencies[i]._id == id) {
							for (var prop in req.body) {

						      if(req.body.hasOwnProperty(prop)){
						      	settings.currencies[i][prop] = req.body[prop]  
						      }
						   }
						   break;
						}
					}
					settings.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok();
					});
				} else {
					res.serverError({name: 'custom', type: 'danger',message: "Settings does not exist"});
				}
			});			
		} else {
			res.notFound();
		}

	},
	destroyCurrencyList: function (req, res) {
			var id = req.param('id');
			
			if (id)	{
				db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
					if (err) return res.negotiate(err);
					if (settings) {

						var currencies = settings.currencies.id(id);
						if(currencies) {
							currencies.remove();
							settings.save(function (err) {
								if (err) return res.validatorError(err);
								res.ok(currencies);
							});
						} else {
							res.notFound();
						}
					} else {
						res.serverError({name: 'custom', type: 'danger', message: "Settings does not exist"});
					}
				});			
			} else {
				res.notFound();
			}
	},
	getCurrencyList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				//sails.log.info(found);
				res.ok(found.currencies)
			}else{
				res.serverError();
			}
		});
	},
	//vessels
	createVesselList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				
				found.vessels.push(req.body);
				found.save(function (err, saved) {
					//sails.log.info('debug:',err);
						if (err) return res.validatorError(err);
						if (saved) {
							var inserted = _.last(saved.vessels);
							res.ok(inserted);								
						} else {
							res.serverError();
						}
				});
			}else{
				
				db.Settings.create({name: 'settings_v1'}, function (err, created) {
					if (err) return res.validatorError(err);
					created.vessels.push(req.body);
					created.save(function (err, saved) {
							if (err) return res.validatorError(err);
							if (saved) {
								var inserted = _.last(saved.vessels);
								res.ok(inserted);								
							} else {
								res.serverError();
							}

					});						
				})
			}
		});
	},
	updateVesselList: function (req, res) {
		var id = req.param('id');
		
		if (id)	{
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
				if (err) return res.negotiate(err);
				if (settings) {
					for (var i = 0; i < settings.vessels.length; i++) {
						
						if (settings.vessels[i]._id == id) {
							for (var prop in req.body) {

						      if(req.body.hasOwnProperty(prop)){
						      	settings.vessels[i][prop] = req.body[prop]  
						      }
						   }
						   break;
						}
					}
					settings.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok();
					});
				} else {
					res.serverError({name: 'custom', type: 'danger',message: "Settings does not exist"});
				}
			});			
		} else {
			res.notFound();
		}

	},
	destroyVesselList: function (req, res) {
			var id = req.param('id');
			
			if (id)	{
				db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
					if (err) return res.negotiate(err);
					if (settings) {

						var vessels = settings.vessels.id(id);
						if(vessels) {
							vessels.remove();
							settings.save(function (err) {
								if (err) return res.validatorError(err);
								res.ok(vessels);
							});
						} else {
							res.notFound();
						}
					} else {
						res.serverError({name: 'custom', type: 'danger', message: "Settings does not exist"});
					}
				});			
			} else {
				res.notFound();
			}
	},
	getVesselList: function (req, res) {
		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				//sails.log.info(found);
				res.ok(found.vessels)
			}else{
				res.serverError();
			}
		});
	},							
};

