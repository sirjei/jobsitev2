/**
 * HistoryController
 *
 * @description :: Server-side logic for managing histories
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	create: function (req, res)	{

		db.Applicant.findOne({user: req.user._id}).exec(function (err, applicant) {
			if (err) res.serverError(err);
			if (applicant) {

				var data = {
					position: req.body.position,
			        agency: req.body.agency,
			        salary: {
				        value: req.body.salary.value,
				        currency: req.body.salary.currency
			        },
	       			signOn: req.body.signOn,
			        signOff: req.body.signOff,
			        vesselName: req.body.vesselName,		        
			        vesselType: req.body.vesselType,
			        route: req.body.route,
			        engineType: req.body.engineType,
			        flag: req.body.flag,
			        principal: req.body.principal,
			        masterNationality: req.body.masterNationality,	
			        grtkw: req.body.grtkw				
		    	};//end of data

				applicant.history.push(data);
				applicant.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok(data);
				});
			} else {
				res.notFound();
			}
		});

	},
	updateOne: function (req, res) {
		var id = req.query.id;

		db.Applicant.findOne({
			user: req.user._id,
			'history._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var history = result.history.id(id);
				if (history) {
					var index = result.history.indexOf(history);
					
					result.history[index].position = req.body.position;
			        result.history[index].agency = req.body.agency;
			        result.history[index].salary = {
				        value: req.body.salary.value,
				        currency: req.body.salary.currency
			        };
	       			result.history[index].signOn = req.body.signOn;
			        result.history[index].signOff = req.body.signOff;
			        result.history[index].vesselName = req.body.vesselName;
			        result.history[index].vesselType = req.body.vesselType;
			        result.history[index].route = req.body.route;
			        result.history[index].engineType = req.body.engineType;
			        result.history[index].flag = req.body.flag;
			        result.history[index].principal = req.body.principal;
			        result.history[index].masterNationality = req.body.masterNationality;
			        result.history[index].grtkw = req.body.grtkw;

					result.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok(result.history[index]);
					});

				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});		
	},
	destroy: function (req, res) {
		var id = req.query.id;
		db.Applicant.findOne({
			user: req.user._id,
			'history._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				result.history.id(id).remove();
				result.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok();
				});				
			}else{
				res.notFound();
			}
		});
	},
	getOne: function (req, res) {
		var id = req.query.id;
		sails.log.info('History:'+id, 'User'+req.user._id)
		db.Applicant.findOne({
			user: req.user._id,
			'history._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var data = result.history.id(id);
				res.ok(data);			
			}else{
				res.notFound();
			}
		});
	},
	getAll: function (req, res) {
		db.Applicant.findOne({
			user: req.user._id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var data = result.history;
				res.ok(data);			
			}else{
				res.notFound();
			}
		});
	}
};

