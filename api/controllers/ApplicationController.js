/**
 * ApplicationController
 *
 * @description :: Server-side logic for managing applications
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	create: function (req, res) {
		var payload = req.body;

		async.waterfall([validateUser, applicationInCompanyExist, jobPostExist, applicationInJobPostExist],
		function (err, applicationData, job) {
		    if (err) {
		    	switch (err.errorType) {
		    		case "serverError":
		    				res.serverError(err.errorMsg);
		    			break;
		    		case "notFound":
		    				res.notFound(err.errorMsg);
		    			break;
		    		case "negotiate":
		    				res.negotiate(err.errorMsg);
		    			break;	
		    		case "badRequest":
		    				res.badRequest(err.errorMsg);
		    			break;			    					    					    			
		    	}
		    } else {
			    //if no error insert in to the array
				job.applications.push(applicationData);
				job.save(function (err, created) {//save and validate
					if (err) return res.validatorError(err)
					res.ok(created);
				});			    	
		    }

		});

		function validateUser (callback) {
			db.Applicant.findOne({user: req.user._id}).exec(function (err, user) {
				if (err) return callback({errorType: "serverError",errorMsg: err})
				if (user) {
					//get the users rank / name
					var applicationData = {
				        	applicant: req.user._id,
				        	rank: user.rank,
				        	name: req.user.name,
					};

					callback(null,applicationData);
				} else {
					callback({errorType: "notFound",errorMsg: ""});
				}
			});	
		}//end of validateUser

		function applicationInCompanyExist (applicationData, callback) {

			db.Job.find({
				agency: payload.agencyId, 
				'applications.applicant': applicationData.applicant
			})
			.exec(function (err, results) {
				if (err) return callback({errorType: "negotiate",errorMsg: err});
				if (results.length) {
					var errorMsg = {name: 'custom', type: 'info', message: 'You currently have an existing application being processed for this company'};
					callback({errorType: "badRequest",errorMsg: errorMsg});
					sails.log.info("APPLICATION IN COMPANY EXIST");
				}else{
					callback(null, applicationData);
					sails.log.info("APPLICATION IN COMPANY EXISTssss");
				}
			});				
		}//end of checkIfAlreadyHaveApplication

		function jobPostExist (applicationData, callback) { //check if job post to apply exists
				sails.log.info("jobPostExist");
			db.Job.findOne({_id: payload.jobId}).exec(function (err, job) {
				if (err) return callback({errorType: "negotiate",errorMsg: err})
				if (!job) return callback({errorType: "notFound",errorMsg: ""});
				//if job exists..  
				callback(null, applicationData, job)
			});//end of find				
		}//end of jobPostExist

		function applicationInJobPostExist (applicationData, job, callback) {
	    	//search if there is already an existing application for the applicant..
	    	for (var i = 0; i < job.applications.length; i++) {
	    		if (job.applications[i].applicant === applicationData.applicant) {
	    			var errorMsg = {name: 'custom', type: 'info', message: 'You already have an existing application being processed'};
	    			return callback({errorType: 'badRequest', errorMsg:errorMsg});
	    		}
	    	};	

	    	callback(null, applicationData, job);		
		}
	},
	///old versions
	getAllByAgency: function (req, res) {
		//find all jobs posted by the agency and get all of the applications
		db.Job.aggregate([
		{ $match:{author: req.user._id} },
		{ $unwind: "$applications" },
		{ $project: {
				_id:0,
				job_id: "$_id",
				application_id: "$applications._id",
				applicant_id: "$applications.applicant_id",
				rank: "$applications.rank",
				name: {
					first: "$applications.name.first",
					last: "$applications.name.last",
				},
				position: "$position",
				status: "$applications.status",
				proposedSalary: "$proposedSalary",				
				expectedSalary: {
					currency: "$proposedSalary.currency",
					value: "$applications.expectedSalary.value" 
				}
			}
		},
		{ $match:{ status: 'pending' } }
		]).exec(function (err, result) {
		    if (err) return res.serverError(err);

		    sails.log.info(result);
		    res.ok(result);
		});
	},
	getAllByApplicant: function (req, res) {
		//find all jobs where the user have an application
		db.Job.aggregate([
		{ $match:{"applications.applicant_id": req.user._id} },
		{ $unwind: "$applications" },
		{ $match:{"applications.applicant_id": req.user._id} },
		{ $project: {
				_id:0,
				job_id: "$_id",
				application_id: "$applications._id",
				author_id: "$author",
				authorName: "$authorName",
				position: "$position",
				status: "$applications.status",
				proposedSalary: "$proposedSalary",
				expectedSalary: {
					currency: "$proposedSalary.currency",
					value: "$applications.expectedSalary.value" 
				}
			}
		},
		{ 
			$match:{ 
				$or:[{status: 'pending'},{status: 'denied'}]
			} 
		}
		]).exec(function (err, result) {
		    if (err) return res.serverError(err);

		    sails.log.info(result);
		    res.ok(result);
		});
	},	
	deny: function (req, res) {
		var id = req.param('id');
		db.Job.findOne({
			author: req.user._id,
			'applications._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var application = result.applications.id(id);
				if (application) {
					var index = result.applications.indexOf(application);
					result.applications[index].status = "denied";

					result.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok(application);
					});
				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});	

	},
	remove: function (req, res) {
		var application_id = req.param('id');//search the application id
		db.Job.findOne({
			'applications._id': application_id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var application = result.applications.id(application_id);
				if (application) {

					if (application.applicant_id.equals(req.user._id)) {// the applicant owns that application
						result.applications.id(application_id).remove();
						result.save(function (err) {
							if (err) return res.validatorError(err);
							res.ok(application);
						});						
					} else {
						res.notFound();	
					}

				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});
	},
	getOne: function (req, res) {
		var id = req.param('id');
		db.Job.findOne({
			'applications._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var application = result.applications.id(id);
				if (application) {
					var data = {
					  _id: application._id,
					  applicant_id: application.applicant_id,
					  rank: application.rank,
					  position: result.position,
					  status: application.status,
					  expectedSalary: {
					    currency: application.expectedSalary.currency,
					    value: application.expectedSalary.value
					  },
					  name: application.name					
					};
					sails.log.info(data);
					res.ok(data);
				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});	
	}	
};

