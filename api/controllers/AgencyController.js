/**
 * AgencyController
 *
 * @description :: Server-side logic for managing agencies
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	updateProfile: function (req, res) {
		db.Agency.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				var data = {
					name: req.body.name,
					landline: req.body.landline,
					mobile: req.body.mobile,	
					fax: req.body.fax,
					websiteUrl: req.body.websiteUrl,
					companyEmail: req.body.companyEmail,
					address: req.body.address,
					city: req.body.city,
					state: req.body.state,
					country: req.body.country,
					zipCode: req.body.zipCode
				};

				_.extend(result, data);//overwrite the current values

				result.save(function (err) {
					if (err) return res.validateError(err);
					res.ok();
				})
			} else {
				res.forbidden();
			}
		});
	},
	updateMap: function (req, res) {
		db.Agency.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				result.map = req.body;
				result.save(function (err) {
					if (err) return res.validateError(err);
					res.ok();
				})
			} else {
				res.forbidden();
			}
		});
	},
	getSocial: function (req, res) {
		db.Agency.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				res.ok(result.socialMedia);
			} else {
				res.forbidden();
			}
		});	
	},
	updateSocial: function (req, res) {
		db.Agency.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				result.socialMedia = req.body.socialMedia;
				result.save(function (err) {
					if (err) return res.validateError(err);
					res.ok();
				});
			} else {
				res.forbidden();
			}
		});		
	},
	getDocs: function (req, res) {
		db.Agency.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				res.ok(result.documents);
			} else {
				res.forbidden();
			}
		});	
	},
	updateDocs: function (req, res) {
		db.Agency.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				result.documents = req.body.documents;
				result.save(function (err) {
					if (err) return res.validateError(err);
					res.ok();
				});
			} else {
				res.forbidden();
			}
		});		
	}	
};

