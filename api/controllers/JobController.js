/**
 * JobController
 *
 * @description :: Server-side logic for managing jobs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var nv = require('node-validator');
module.exports = {
	create: function (req, res) {
		sails.log.info(req.user.agency);
		db.Agency.findOne({_id: req.user.agency._id}).exec(function (err, agency) {
			if (err) return res.serverError(err);

			if (agency) {

				var data = {
					agency: agency._id,//include the agency id
					postedBy: req.user._id,
					agencyName: agency.name,
					postedByName: req.user.name,
					country: agency.country,//make country is specified
					headline: req.body.headline,
					position: req.body.rank,    
					urgent: req.body.urgent,    
			        // openUntil: req.body.openUntil,
			        // vesselName: req.body.vesselName,
			        vesselType: req.body.vesselType,
			        proposedSalary: req.body.proposedSalary,
			        // details: req.body.details
			        trainings: req.body.trainings,
			        employmentBenefits: req.body.benefits,
			        qualifications: req.body.qualifications,
			        keywords: []
				};

				//start creating keywords
				if (!_.isNull(data.position) && !_.isUndefined(data.position) && _.isString(data.position)) {
					data.position = data.position.toLowerCase();
					data.position = data.position.split(" ");
					data.keywords.concat(data.position);
				}

				if (!_.isNull(data.vesselType) && !_.isUndefined(data.vesselType) && _.isString(data.vesselType)) {
					data.vesselType = data.vesselType.toLowerCase();
					data.vesselType = data.vesselType.split(" ");
					data.keywords.concat(data.vesselType);

				}				 
				
				if (!_.isNull(data.agencyName) && !_.isUndefined(data.agencyName) && _.isString(data.agencyName)) {
					data.agencyName = data.agencyName.toLowerCase();
					data.agencyName = data.agencyName.split(" ");
					data.keywords.concat(data.agencyName);
				}				
				if (!_.isNull(data.headline) && !_.isUndefined(data.headline) && _.isString(data.headline)) {
					var headline = data.headline.toLowerCase();
					headline = headline.replace(/(\b(\w{1,3})\b(\s|$))/g,'').split(" ");
					data.keywords.concat(headline);
				}
				//end of creating keywords
				db.Job.create(data,function(err, created) {
					if (err) return res.validatorError(err);
					res.ok(created);
				});

			} else {
				return res.notFound();
			}

		});

	},
	getOne: function (req, res) {
		var id = req.query.id;

		db.Job.findOne({_id: id}).populate('agency','photo country').exec(function (err, results) {
			if (err) return res.negotiate(err);
			if (results)  return res.ok(results);
			res.notFound({});
		});		
	},
	getAutoSuggestions: function (req, res) {


		async.parallel([headlineList,agencyList],
		// optional callback
		function(err, results){
		    var suggestions = results[0].concat(results[1]);
		    res.ok(suggestions);
		});

		function headlineList (callback) {
			//get job headlines
			var headlines = [];
			db.Job.find({},'-_id headline').exec(function (err, results) {
				if (err) return callback(null,headlines);
				if (results)  {
					for (var i = 0; i < results.length; i++) {
						headlines.push(results[i].headline);
					};
					callback(null,headlines);
				} else {
					callback(null,headlines);
				}
			});	
		}

		function agencyList (callback) {
			//get agencies
			var agencies = [];
			db.Agency.find({},'-_id name').exec(function (err, results) {
				if (err) return callback(null,agencies);
				if (results)  {
					for (var i = 0; i < results.length; i++) {
						agencies.push(results[i].name);
					};
					callback(null,agencies);
				} else {
					callback(null,agencies);
				}
				
			});
		}

	},
	createKeywords: function (req, res) {
		var currentItem, agencyName, vesselType, position, erroCtr = 0;

		db.Job.find({}).exec(function (err, results) {
			if (err) return res.serverError(err)
			if (results)  {
				for (var i = 0; i < results.length; i++) {
					
					results[i].keywords = results[i].headline.replace(/(\b(\w{1,3})\b(\s|$))/g,'').split(" ");
					agencyName = results[i].agencyName.split(" ");
					vesselType = results[i].vesselType.split(" ");
					position = results[i].position.split(" ");

					results[i].keywords = results[i].keywords.concat(agencyName,vesselType,position);

					results[i].save(function (err) {
						if (err) {
							erroCtr++;
						}
					});	
				};
				sails.log.info("errors:",erroCtr);
				res.ok();

			} else {
				res.ok(results);
			}
		});	

	},
	/////////////// old versions	
	updateOne: function (req, res) {
		//revisit: update only if the user owns the job post
		var id = req.param('id');
		//prep data that should only be updated
		var data = {
			position: req.body.position,   
			//datePosted: req.body.datePosted,     
	        openUntil: req.body.openUntil,
	        vesselName: req.body.vesselName,
	        vesselType: req.body.vesselType,
	        proposedSalary: req.body.proposedSalary,
	        details: req.body.details
		};



		db.Job.findOne({_id: id},'-applications').exec(function(err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				sails.log.info(result);
				sails.log.info(result.author+ ' - '+req.user._id);
				
				if (result.author.equals(req.user._id)) {//check if the job post is owned by the editor
					//iterate the data and change the value of the corresponding fields
					_.extend(result, data) 
					//then save the changes
					result.save(function (err, updated) {
						if (err) return res.validatorError(err);
						res.ok(updated);
					});	
				} else {
					res.forbidden({error: 'custom', message: 'You are not allowed to edit this post'});
				}
			
			} else {
				res.badRequest({error: 'custom', message: 'does not exist'});
			}

		});
	},

	getAllPublic: function (req, res) {
			var query = {};

			if (req.query.searchBy == 'company') {
				query = {
					$and: [
						{'authorName': req.query.keyword},
						{'flag': req.query.country}
					]
				};
			} else if (req.query.searchBy == 'position') {
				query = {
					$and: [
						{'position': req.query.keyword},
						{'flag': req.query.country}
					]
				};				
			}


			db.Job.find(query).exec(function(err, results) {
				if (err) return res.negotiate(err);
				//res.ok(results);
				async.map(results, function (item, callback) {
					
					var data = {
						_id: item._id,
					    author: item.author,
					    authorName: item.authorName,
					    flag: item.flag,
					    position: item.position,
					    openUntil: item.openUntil,
					    proposedSalary: {
					      currency: item.proposedSalary.currency,
					      value: item.proposedSalary.value
					    },
					    vesselType: item.vesselType,
					    vesselName: item.vesselName,
					    datePosted: item.datePosted,
					    haveApplication: false,
					    haveInterview: false
					  };

					  if (req.isAuthenticated()) {//if logged in
					  	
					  	if (req.user.accountType == 'applicant') {//if an applicant
					  		  //search if the viewer already has an application
						  // if (item.applications.id(req.user._id) || item.interviews.id(req.user._id)) {
						  // 		data.haveApplication = true;
						  // }

					    	for (var i = 0; i < item.applications.length; i++) {
					    		if (item.applications[i].applicant_id.equals(req.user._id)) {
					    			data.haveApplication = true;
					    			break;
					    		}
					    	};

					    	for (var i = 0; i < item.interviews.length; i++) {
					    		if (item.interviews[i].applicant_id.equals(req.user._id)) {
					    			data.haveInterview = true;
					    			break;
					    		}
					    	};					    							  
					  	}
					  }

					  callback(null, data);
				}, function(err, results){
    				res.ok(results);
				});
			});	

				
		

	},
	getAllByAgency: function (req, res) {
		db.Job.find({author: req.user._id},'-applications -interviews').exec(function(err, results) {
			if (err) return res.negotiate(err);
			res.ok(results);
		});
	},	
	getOneByAgency: function (req, res) {
		var id = req.param('id');

		db.Job.findOne({_id: id, author: req.user._id},'-applications -interviews').exec(function(err, results) {
			if (err) return res.negotiate(err);
			if (results)  return res.ok(results);
			res.notFound();
		});
	},		
	destroyOne: function (req, res) {
		var id = req.param('id');
		//revisit: make sure that it can only be destroyed by the author
		db.Job.findById(id).exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				if (result.author.equals(req.user._id)) {
					result.remove(function (err) {
						if (err) return negotiate(err);
						res.ok();
					});					
				} else {
					res.forbidden({error: 'custom', message: 'You are not allowed to delete this post'});	
				}
			} else {
				res.badRequest({error: 'custom', message: 'does not exist'});
			}
		});
	},
	destroyAll: function (req, res) {

	},

};

