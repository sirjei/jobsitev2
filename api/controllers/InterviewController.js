/**
 * InterviewController
 *
 * @description :: Server-side logic for managing interviews
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	set: function (req, res) {
		//need application id for deletion and transfer to interview sub document

		//check first 
		db.Job.findOne({
			'applications._id': req.body._id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				//check if the applicant is already set for interview
				for (var i = 0; i < result.interviews.length; i++) {
					if (result.interviews[i].applicant_id.equals(req.body.applicant_id)){
						return res.badRequest({error:"custom",message:"Applicant is already on schedule for interview"})
					}
				}
				//if not yet.. start to delete the application then transfer the interview
				result.applications.id(req.body._id).remove();

				var data = {
		        	applicant_id: req.body.applicant_id,
		        	rank: req.body.rank,	        	 
		        	name: req.body.name,
			        schedule: req.body.schedule,
			        expectedSalary: req.body.expectedSalary
				};

				result.interviews.push(data);
				result.save(function (err) {
					//if deleted.. insert the interview data
					if (err) return res.validatorError(err);
					res.ok();
				});


			}else{
				res.notFound();
			}
		});			
	},
	getAllByAgency: function (req, res) {

		//find all jobs posted by the agency and get all of the applications
		db.Job.aggregate([
		{ $match:{author: req.user._id} },
		{ $unwind: "$interviews" },
		{ $project: {
				_id:0,
				job_id: "$_id",
				interview_id: "$interviews._id",
				applicant_id: "$interviews.applicant_id",
				rank: "$interviews.rank",
				name: {
					first: "$interviews.name.first",
					last: "$interviews.name.last",
				},
				position: "$position",
				status: "$interviews.status",
				schedule: "$interviews.schedule",
				proposedSalary: "$proposedSalary",				
				expectedSalary: {
					currency: "$proposedSalary.currency",
					value: "$interviews.expectedSalary.value" 
				}
			}
		},
		{ $match:{ status: 'on schedule' } }
		]).exec(function (err, result) {
		    if (err) return res.serverError(err);
		    res.ok(result);
		});	
	
	},
	getAllByApplicant: function (req, res) {

		//find all jobs posted by the agency and get all of the applications
		db.Job.aggregate([
		{ $match:{"interviews.applicant_id": req.user._id} },
		{ $unwind: "$interviews" },
		{ $match:{"interviews.applicant_id": req.user._id} },
		{ $project: {
				_id:0,
				job_id: "$_id",
				interview_id: "$interviews._id",
				author_id: "$author",
				authorName: "$authorName",
				position: "$position",
				status: "$interviews.status",
				schedule: "$interviews.schedule",
				proposedSalary: "$proposedSalary",
				expectedSalary: {
					currency: "$proposedSalary.currency",
					value: "$interviews.expectedSalary.value" 
				}
			}
		},
		]).exec(function (err, result) {
		    if (err) return res.serverError(err);

		    sails.log.info(result);
		    res.ok(result);
		});
	
	},
	cancel: function (req, res) {
		var id = req.param('id');
		db.Job.findOne({
			author: req.user._id,
			'interviews._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var interview = result.interviews.id(id);
				if (interview) {
					var index = result.interviews.indexOf(interview);
					result.interviews[index].status = "cancelled";

					result.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok(interview);
					});

				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});			
	},
	remove: function (req, res) {
		var id = req.param('id');//search the application id
		db.Job.findOne({
			'interviews._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var interview = result.interviews.id(id);
				if (interview) {

					if (interview.applicant_id.equals(req.user._id)) {// the applicant owns that application
						result.interviews.id(id).remove();
						result.save(function (err) {
							if (err) return res.validatorError(err);
							res.ok(interview);
						});						
					} else {
						res.notFound();	
					}

				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});
	},	
	resched: function (req, res) {
		var id = req.param('id');
		db.Job.findOne({
			author: req.user._id,
			'interviews._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var interview = result.interviews.id(id);
				if (interview) {
					var index = result.interviews.indexOf(interview);
					result.interviews[index].schedule = req.body.schedule;

					result.save(function (err) {
						if (err) return res.validatorError(err);
						res.ok(interview);
					});

				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});		
	},
	getOne: function (req, res) {
		var id = req.param('id');
		db.Job.findOne({
			author: req.user._id,
			'interviews._id': id
		})
		.exec(function (err, result) {
			if (err) return res.negotiate(err);
			if (result) {
				var interview = result.interviews.id(id);
				if (interview) {
					interview.position = result.position;
					res.ok(interview);

					
				} else {
					res.notFound();	
				}
			}else{
				res.notFound();
			}
		});		
	}
};

