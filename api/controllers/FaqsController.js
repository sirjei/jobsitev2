/**
 * FaqsController
 *
 * @description :: Server-side logic for managing faqs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	//get training list
	getAll: function (req, res) {
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
			 	if (err) return res.validatorError(err);
				if(found){
					//sails.log.info(found);
					res.ok(found.faq)

				}else{
					res.serverError();
				}
			});
	},
	getOne: function (req, res) {
		var id = req.param('id');

		db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
		 	if (err) return res.validatorError(err);
			if(found){
				//sails.log.info(found);
				var faq = found.faq.id(id);
				if (faq) {
					res.ok(faq);
				} else {
					res.notFound();
				}
				

			}else{
				res.serverError();
			}
		});
	},			
	create: function (req, res) {

			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, found) {
			 	if (err) return res.validatorError(err);
				if(found){
					
					found.faq.push(req.body);
					found.save(function (err, saved) {
						//sails.log.info('debug:',err);
							if (err) return res.validatorError(err);
							if (saved) {
								var inserted = _.last(saved.faq);
								res.ok(inserted);								
							} else {
								res.serverError();
							}
					});
				}else{
					db.Settings.create({name: 'settings_v1'}, function (err, created) {
						if (err) return res.validatorError(err);
						created.faq.push(req.body);
						created.save(function (err, saved) {
								if (err) return res.validatorError(err);
								if (saved) {
									var inserted = _.last(saved.faq);
									res.ok(inserted);								
								} else {
									res.serverError();
								}
						});						
					})
				}
			});
	},
	update: function (req, res) {
		var id = req.param('id');
		
		if (id)	{
			db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
				if (err) return res.negotiate(err);
				if (settings) {
					// for (var i = 0; i < settings.faq.length; i++) {
						
					// 	if (settings.faq[i]._id == id) {
					// 	   //CollectionService.overwrite(req.body, settings.faq[i]);
					// 	   _.extend(settings.faq[i], req.body);//overwrite the current values
					// 	}
					// }
					var faq = settings.faq.id(id);//find
					if (faq) {
						//get index
						var index = settings.faq.indexOf(faq);
						_.extend(settings.faq[index], req.body);//overwrite the current values
						
						settings.save(function (err) {
							if (err) return res.validatorError(err);
							res.ok();
						});						
					} else {
						res.notFound();
					}


				} else {
					res.serverError({name: 'custom', type: 'danger', message: "Settings does not exist"});
				}
			});			
		} else {
			res.notFound();
		}

	},
	destroy: function (req, res) {
			var id = req.param('id');
			
			if (id)	{
				db.Settings.findOne({name: 'settings_v1'}).exec(function (err, settings) {
					if (err) return res.negotiate(err);
					if (settings) {

						var faq = settings.faq.id(id).remove();
						settings.save(function (err) {
							if (err) return res.validatorError(err);
							res.ok(faq);
						});
					} else {
						res.serverError({name: 'custom', type:'danger', message: "Settings does not exist"});
					}
				});			
			} else {
				res.notFound();
			}
	},	
};

