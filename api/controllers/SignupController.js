/**
 * SignupController
 *
 * @description :: Server-side logic for managing signups
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	applicant: function (req, res) {

		async.waterfall([step1, step2/*, step3*/,login], function (err, result) {
		   // result now equals 'done'    
		   if (err) {
		   		if (err.name == "serverError") {
		   			res.serverError(err);
		   		}
		   		if (err.name == "ValidationError" || err.name == "MongoError") {
		   			res.validatorError(err);
		   		}		   		
		   } else {
		   	res.ok(result);
		   }
		});  

		function step1(callback) {
			req.body.accountType = 'applicant';
			    //create password before creation
	        var salt = EncryptionService.createSalt(),
	            hashPwd = EncryptionService.hashPwd(salt,req.body.password), 
	            hashActivation = EncryptionService.randomValueHex(12);
	        req.body.salt = salt;
	        req.body.password = hashPwd;
	        req.body.activationCode = hashActivation;
		        
			db.User.create(req.body, function (err, user) {
				if (err) {
					callback(err);
				} else {
					callback(null, user);
				}
			});
		}//end of step1

		function step2(user, callback) {
			//start inserting applicant data
			req.body.user = user._id;
			
			db.Applicant.create(req.body, function (err, applicant) {
				if (err) {//undo the user insertion
					user.remove(function (removeErr, removedUser) {
						if (removeErr) return callback(removeError);
						callback(err);
					});							
				} else {
					//update the user collection for the association of the applicant collection
					db.User.findByIdAndUpdate(user._id, {applicant:applicant._id}).exec(function (err, updated) {
						if (err) return callback(err);
						//res.ok();
						callback(null, user, applicant);
					});
				}
			});
		}

		function step3(user, applicant, callback) {
            //send the activation code
            var protocol = req.connection.encrypted?'https':'http';
            var baseUrl = protocol + '://' + req.headers.host + '/'; 
            var emailContent = {
                name: req.body.name.first+' '+req.body.name.last,
                activationLink: baseUrl+'#/activation?token='+user.activationCode
            };

            sails.renderView("email",emailContent, function(err,html){
            	var emailResult = {status: "", message: ""};
                if (err) {
                    sails.log.error('Sending Error:',err);
                    emailResult.status = "error";
                    emailResult.message = err;
                    callback(null,{registration: "success", email: emailResult});
                } else {
                    var nodemailer = require('nodemailer');
                    var transporter = nodemailer.createTransport(sails.config.email.gmail);
                    var mailOptions = {
                        from: 'Admin <www.marinosaluneta.com>', // sender address
                        to: user.email, // list of receivers
                        subject: 'Activate Account', // Subject line
                        html: html // html body
                    };
                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, function(errorSending, info){
                    	
                        if(errorSending){
                            sails.log.error('Sending Error:',errorSending);
                            emailResult.status = "error";
                            emailResult.message = errorSending;
                        }else{
                            sails.log.info('Message sent: ' + info.response);
                            emailResult.status = "success";
                            emailResult.message = info.response;                            

                        }
                        callback(null,{registration: "success", email: emailResult});
                    });                
                }
            });	
		}

		function login (user, applicant, callback) {
		/*
		* login the user after registration
		*/
          req.logIn(user,function(err){
               if(err){
                	callback(err); 
               } else {
                    db.User.findOne({_id: user._id},'accountType applicant').populate('applicant', '_id name photo').exec(function(err,user){
                       if (err) return callback(err);
                        callback(null, user);
                    });
               }
            });   
		}//end of login

          	
	},
	agency: function (req, res) {

		async.waterfall([step1, step2/*, step3*/, login], function (err, result) {
		   // result now equals 'done'    
		   if (err) {
		   		sails.log.info(err);
		   		if (err.name == "serverError") {
		   			res.serverError(err);
		   		}
		   		if (err.name == "ValidationError" || err.name == "MongoError") {
		   			res.validatorError(err);
		   		}			   		
		   } else {
		   	res.ok();
		   }
		}); 

		function step1(callback) {

			var salt = EncryptionService.createSalt(),
	            hashPwd = EncryptionService.hashPwd(salt,req.body.password), 
	            hashActivation = EncryptionService.randomValueHex(12);
		        
			var data = {
				email: req.body.email,
				password: hashPwd,
				accountType: 'agency',
				name: req.body.adminName,
				role: 'administrator',
				salt: salt,
				activationCode: hashActivation,
			    //create password before creation
			};

			db.User.create(data, function (err, user) {
				if (err) {
					callback(err);
				} else {
					callback(null, user);
				}				
			});
		}

		function step2(user, callback) {
			
			var data = {
				name: req.body.companyName,
				companyEmail: req.body.email,
				country: req.body.country
			};

			db.Agency.create(data, function (err, agency) {
				if (err) {//undo the user insertion
					user.remove(function (removeErr, removedUser) {
						if (removeErr) return callback(removeErr);
						callback(err);
					});							
				} else {

					db.User.findByIdAndUpdate(user._id, {agency:agency._id}).exec(function (err, updated) {
						if (err) return callback(err);
						callback(null, user, agency);
					});

				}
			});
		}

		function step3(user, agency, callback) {
            //send the activation code
            var protocol = req.connection.encrypted?'https':'http';
            var baseUrl = protocol + '://' + req.headers.host + '/'; 
            var emailContent = {
                name: req.body.name,
                activationLink: baseUrl+'#/activation?token='+user.activationCode
            };

            sails.renderView("email",emailContent, function(err,html){
            	var emailResult = {status: "", message: ""};
                if (err) {
                    sails.log.error('Sending Error:',err);
                    emailResult.status = "error";
                    emailResult.message = err;
                    callback(null,{registration: "success", email: emailResult});
                } else {
                    var nodemailer = require('nodemailer');
                    var transporter = nodemailer.createTransport(sails.config.email.gmail);
                    var mailOptions = {
                        from: 'Admin <www.marinosaluneta.com>', // sender address
                        to: user.email, // list of receivers
                        subject: 'Activate Account', // Subject line
                        html: html // html body
                    };
                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, function(errorSending, info){
                    	
                        if(errorSending){
                            sails.log.error('Sending Error:',errorSending);
                            emailResult.status = "error";
                            emailResult.message = errorSending;
                        }else{
                            sails.log.info('Message sent: ' + info.response);
                            emailResult.status = "success";
                            emailResult.message = info.response;                            

                        }
                        callback(null,{registration: "success", email: emailResult});
                    });                
                }
            });	
		}

		function login (user, agency, callback) {
		/*
		* login the user after registration
		*/
          req.logIn(user,function(err){
               if(err){
                	callback(err); 
               } else {
                    db.User.findOne({_id: user._id},'accountType agency').populate('agency', 'id map name photo').exec(function(err,user){
                       if (err) return callback(err);
                        callback(null, user);
                    });
               }
                 
            });   
		}//end of login

		
	},
	admin: function (req, res) {

		req.body.role = 'superadmin';
		db.Administrator.create(req.body, function (err, admin) {
			if (err) {
				res.validatorError(err);
			} else {
				res.ok({message: 'admin created'});
			}
		});
	}
};

