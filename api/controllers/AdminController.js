/**
 * AdminController
 *
 * @description :: Server-side logic for managing admins
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
	var passport = require('passport');
module.exports = {
  login: function (req, res) {
        var auth = passport.authenticate('admin',function(err,admin,status){
            if(err){
                res.serverError(err);
            } else {
                if(!admin){
                    if (err) return res.serverError(err);  
                    if (status) return res.badRequest(status);
                    
                }else{
                    req.logIn(admin,function(err){
                       if(err){
                          res.negotiate(err);
                       } else {
                          res.ok(admin);
                       }
                    });  
                }            
            }
        });
        auth(req,res);    
  }
};

