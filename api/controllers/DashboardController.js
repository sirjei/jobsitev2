/**
 * DashboardController
 *
 * @description :: Server-side logic for managing dashboards
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	agencyStats: function (req, res) {

		async.parallel({
		    accountStats: function(callback){
				db.Agency.findOne({user: req.user._id}).exec(function (err, result) {
				 	if (err) return callback({error: "serverError", message: err});
					if (result) {
						//followers
						callback(null,{followers: result.followers.length});				
					} else {
						callback({error: "notFound", message: "User not found"});
					}
				});//end of agency query
		    },
		    jobStats: function(callback){
				//count the applicants (applications + interview scheds) and job posted
				db.Job.aggregate([
					{
						$match: {author: req.user._id}//filter first all of the jobs that the company have posted
					},{
						$project: {
							_id: 0,
							job_id: "$_id",
							author: "$author",
							applicants: { 
								$add : [ 
									{$size: "$applications"}, 
									{$size: "$interviews"},  
								] 
							}
						}
					},
					{
						$group: {
							_id: "$author",//group by author to count the job post because it is already filtered
							jobsPosted: {$sum: 1},//count job posted
							totalApplicants: {$sum: "$applicants"},
						}
					},
				]).exec(function (err, results) {
					if (err) return callback({error: "serverError", message: err});
					callback(null, results[0]);
				});//end of aggregate	
		    }
		},
		function(err, results) {
		    if (err) {
		    	if (err.error == 'serverError') {
		    		res.serverError(err);
		    	}
		    	if (err.error == 'notFound') {
		    		res.notFound(err);
		    	}		    	
		    } else {
		    	res.ok(results);
		    }
		});			
	},
	applicantStats: function (req, res) {


		async.parallel({
		    accountStats: function(callback){
				db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
				 	if (err) return callback({error: "serverError", message: err});
					if (result) {
						//followed
						callback(null, {followed: result.followed.length});				
					} else {
						callback({error: "notFound", message: "User not found"});
					}
				});//end of agency query
		    },
		    postStats: function(callback){
		    	//count the applications & scheduled interviews
				db.Job.find({
					$or: [ { 'applications.applicant_id': req.user._id }, { 'interviews.applicant_id': req.user._id } ]
				}).exec(function (err, jobs) {
					if (err) return callback({error: "serverError", message: err});
					var count = {
						applications: 0,
						interviews: 0
					};
					async.map(jobs, function (job, callback) {


						for (var i = 0; i < job.applications.length; i++) {
							if (job.applications[i].applicant_id.equals(req.user._id)) {
								count.applications++;
								break;
							}
						};

						for (var i = 0; i < job.interviews.length; i++) {
							if (job.interviews[i].applicant_id.equals(req.user._id)) {
								count.interviews++;
								break;
							}
						};

						callback(null);
					}, function(err, results){
					    // results is now an array of stats for each file
					    if (err) return callback(err);
					    callback(null, count);

					});			
				});
		    }
		},
		function(err, results) {
		    if (err) {
		    	if (err.error == 'serverError') {
		    		res.serverError(err);
		    	}
		    	if (err.error == 'notFound') {
		    		res.notFound(err);
		    	}		    	
		    } else {
		    	res.ok(results);
		    }
		});		

	}
};

