/**
 * ApplicantController
 *
 * @description :: Server-side logic for managing applicants
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
module.exports = {
	getFullCVDetails: function (req, res) {
		db.Applicant.findOne({user: req.user._id})
		.populate('user', 'name email')
		.exec(function (err, details) {
			if (details) {
				res.ok(details)
			} else {
				res.notFound();
			}
		});
	},
	basicInfo: function (req, res) {
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				
				var data = {
					rank: req.body.rank,
					name: req.body.name,
					nationality: req.body.nationality,
					dob: req.body.dob,
					gender: req.body.gender,
					contactOne: req.body.contactOne,
					contactTwo: req.body.contactTwo,
					expectedSalaryRange: req.body.expectedSalaryRange,	
					address: req.body.address,
					city: req.body.city,
					state: req.body.state,
					country: req.body.country,
					zipCode: req.body.zipCode
				};

				_.extend(result, data);//overwrite the current values

				result.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok();
				})
			} else {
				res.notFound();
			}
		});
	},	
	primaryDocs: function (req, res) {
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				result.documents = req.body.documents;
				result.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok();
				});
			} else {
				res.notFound();
			}
		});		
	},
	updateOnePrimaryDoc: function (req, res) {

		var code = req.query.code;
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				//check if the document exists

				var found = false;
				for (var i = 0; i < result.documents.length; i++) {
					if (result.documents[i].code === code) {
						result.documents[i].description = req.body.description;
						result.documents[i].number = req.body.number;
						result.documents[i].dateIssued = req.body.dateIssued;
						result.documents[i].validUntil = req.body.validUntil;
						found = true;
					}
				};

				if (!found) {//if not found create it
					result.documents.push(req.body);
				}

				result.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok();
				});
			} else {
				res.forbidden();
			}
		});		
	},	
	trainings: function (req, res) {
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				result.trainings = req.body.trainings;
				result.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok();
				});
			} else {
				res.notFound();
			}
		});		
	},			
	/////////////////old version
	updateProfile: function (req, res) {
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				
				var data = {
					rank: req.body.rank,
					name: {
						first: req.body.name.first,
						last: req.body.name.last,
						full: req.body.name.first+ ' ' + req.body.name.last,
					},
					nationality: req.body.nationality,
					dob: req.body.dob,
					gender: req.body.gender,
					landline: req.body.landline,
					mobile: req.body.mobile,	
					address: req.body.address,
					city: req.body.city,
					state: req.body.state,
					country: req.body.country,
					zipCode: req.body.zipCode
				};

				_.extend(result, data);//overwrite the current values

				result.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok();
				})
			} else {
				res.forbidden();
			}
		});
	},
	getPrimaryDocs: function (req, res) {
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				res.ok(result.documents);
			} else {
				res.forbidden();
			}
		});	
	},
	updatePrimaryDocs: function (req, res) {
		db.Applicant.findOne({user: req.user._id}).exec(function (err, result) {
			if (err) return res.serverError(err);
			if (result) {
				result.documents = req.body.documents;
				result.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok();
				});
			} else {
				res.forbidden();
			}
		});		
	},
	follow: function (req, res) {
		var id = req.param('id');
		db.Agency.findOne({user: id}).exec(function (err, agency) {
			if (err) res.serverError(err);
			if (agency) {
				//check if already following
				var result = {follow:false};
				var following = agency.follow.id(req.user._id);
				if (following) {
					agency.follow.id(req.user._id).remove();
					result.follow = false;
				} else {
					var data = {
						_id: req.user._id
					};
					agency.follow.push(data);
					result.follow = true;
				}

				agency.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok(result);
				})
			} else {
				res.notFound();
			}
		});
	},
	rate: function (req, res) {
		var id = req.param('id');
		db.Agency.findOne({user: id}).exec(function (err, agency) {
			if (err) res.serverError(err);
			if (agency) {
				//check if already following
				var rate = agency.ratings.id(req.user._id);
				if (rate) {//if there is a rating already
					var index = agency.ratings.indexOf(rate);
					agency.ratings[index].score = req.body.rating;
				} else {//if does not exist yet
					var data = {
						_id: req.user._id,
						score: req.body.ratings
					};
					agency.ratings.push(data);//insert
				}

				agency.save(function (err) {
					if (err) return res.validatorError(err);
					res.ok({rating: req.body.rating});
				})
			} else {
				res.notFound();
			}
		});		
	}
	
};

