/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	index: function (req, res) {
		var baseUrl = sails.config.path.baseUrl(req);
		//sails.log.info(baseUrl);
		res.view('index',{bootstrappedUser: req.user,bootstrappedInstance:req.session._csrfSecret,baseUrl:baseUrl });
		//res.view('index');
	},
	account: function (req, res) {
		var baseUrl = sails.config.path.baseUrl(req);
		
		if (req.isAuthenticated()) {

			switch (req.user.accountType) {
				case "applicant":
					res.view('applicant',{bootstrappedUser: req.user,bootstrappedInstance:req.session._csrfSecret,baseUrl:baseUrl });
					break;
				case "agency":
					res.view('agency',{bootstrappedUser: req.user,bootstrappedInstance:req.session._csrfSecret,baseUrl:baseUrl });
					break;	
				default:
					res.redirect('/');
					break;
			}
			
		} else {
			res.redirect('/');
		}
		//res.view('index',{bootstrappedUser: req.user,bootstrappedInstance:req.session._csrfSecret });
		
	},	
	admin: function (req, res) {
		res.ok();
	},
	fourOhFour: function (req, res) {
		res.notFound();
	}
};

