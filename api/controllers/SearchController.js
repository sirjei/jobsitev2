/**
 * SearchController
 *
 * @description :: Server-side logic for managing searches
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	jobs: function (req, res) {
		var param = req.query;
		
		if (param) {//if param has properties
			sails.log.info("PARAMS:",param);
		} else {
			sails.log.info("PARAMS IS EMPTY",param);
		}
		var query = {}

		if (_.has(param, "keywords")) {
			//query["$text"] = { $search: param["keywords"]};
			if (_.isString(param["keywords"])) {
				var keywords = param["keywords"].toLowerCase();
				 keywords = keywords.replace(/(\b(\w{1,3})\b(\s|$))/g,'').split(" ");
			}
			
			query["keywords"] = { $all: keywords };
		}
		
		//check if filter params exists
		if (_.has(param, "agencyName") || _.has(param, "country") || _.has(param, "vesselType") || _.has(param, "position")) {
			query["$and"] = [];
			for (var prop in param) {
				//start construction something like this
				// $and: [
				// 	{'agencyName': req.query.keyword},
				// 	{'country': req.query.country}
				// ]	

				if (typeof param[prop] !== undefined && typeof param[prop] !== null) {

					if (prop != "sortBy" && prop != "keywords") {//do not include sortBy in the main query

						if (param[prop] != "worldwide" && param[prop] != "all positions" && param[prop] != "all vessels") {
							var data = {};

							data[prop] = param[prop].toLowerCase();
							query["$and"].push(data);
						}

					}//end of sortBy check

				}//end of undefined check

			};//end of loop			
		}



		sails.log.info("Current Query:",query);
		
	

		var jobsQuery = db.Job.find(query);

		if (_.has(param, "sortBy")) {//check if params have sortBy
			if (param["sortBy"] == "highest salary") {
				jobsQuery.sort({"proposedSalary.max": -1});
			}

			if (param["sortBy"] == "urgency") {
				jobsQuery.sort({urgent: -1});
			}

			if (param["sortBy"] == "postDate") {
				jobsQuery.sort({datePosted: -1});
			}			
		}

		jobsQuery.populate('agency','photo');
		// jobsQuery.skip(param.skip);
		// jobsQuery.limit(5);
		jobsQuery.exec(function (err, results) {
			if (err) return res.serverError(err);
			var response = {searchResults: results, currentSkip: parseInt(param.skip)};
			res.ok(response);
		});
		
	}
};

