(function (){
	'use script';
angular.module('app.smoothscroll',[])	
	.directive('anchorScroll', anchorScroll);

	anchorScroll.$inject = ['$log', '$location'];
	function anchorScroll ($log, $location) {
		return {
			restrict : "AE",
			link: function (scope, elem, attr) {
				var target = scope.$eval(attr.anchorScroll);
				
				elem.bind('click', function () {
					$log.info(target);
					$('body').scrollTo(target);
				});
				
			}
		}


	}//end of smoothscroll

})();