(function () {
	'use strict';
	//userservice.js
	angular.module('core')
		.factory('userService', userService);

	userService.$inject = ['$log'];
	function userService ($log) {
		var currentUser;

		return {
		    setCurrent: setCurrent,
			getCurrent: getCurrent,
			clearCurrent: clearCurrent,
			getName: getName,
			getAgencyName: getAgencyName
		};

		//////////////
		function setCurrent (user) {
			currentUser = user;
		}

		function getCurrent () {
			return currentUser;
		}

		function clearCurrent () {
			currentUser = undefined;
		}

		function getName () {
			return currentUser ? currentUser.name : "unknown";
		}

		function getAgencyName () {
			if (angular.isDefined(currentUser)) {
				return  currentUser.accountType == "agency" ? currentUser.agency.name : "unknown";
			} else {
				return "unknown";
			}
		}

	}//end of csrfInterceptor

})();