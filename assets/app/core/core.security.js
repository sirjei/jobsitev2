(function () {
	'use strict';
	//core.security.js
	angular.module('core')
		.factory('csrfInterceptor', csrfInterceptor)
		.factory('authService', authService);

	authService.$inject = ['$http', '$log', 'userService'];
	function authService ($http, $log, userService) {

		return {
			login: login,
			logout: logout,
			isAuthenticated: isAuthenticated,
			isLoggedIn: isLoggedIn,
			setBootstrappedUser: setBootstrappedUser
		};

		//////////////////////
	    function login (credentials) {
			var login = $http.post('api/v1/login',credentials)
			login.success(function(currentUser){
				userService.setCurrent(currentUser);
			});

			return login;
	    };

	    function logout () {
			var logout = $http.post('api/v1/logout',{logout:true}).success(function(currentUser){
				userService.clearCurrent();
			});

			return logout;
	    }
	  //   this.adminLogin = function (credentials) {
			// var login = $http.post('api/v1/admin/login',credentials)
			// login.success(function(currentUser){
			// 	userService.setCurrent(currentUser);
			// });
			// return login;
	  //   };

	  	function isAuthenticated ($q, urlHelper) {
	  		//this method is for checking the logged in user inside a resolve
	    	var defer = $q.defer();
	    	if (userService.getCurrent()) {
	    		//reject and issue a redirection inside a $stateChangeError
	    		defer.reject({
	    			action: "redirect",
	    			redirect: {
	    				type: "url",
	    				url: urlHelper.account
	    			}
	    		});	    		
	    	} else {
	    		defer.resolve();
	    	}

	    	return defer.promise;
	    }	

	    function isLoggedIn () {
	    	return userService.getCurrent() || false;
	    }

	    function setBootstrappedUser (user)	 {
	    	userService.setCurrent(user);
	    }
	}//end of authService

	csrfInterceptor.$inject = ['$q', '$injector'];
	function csrfInterceptor ($q, $injector) {
	    var _token = false;
		var interceptor = {
			request: request
		};
		return interceptor;

		///////

		function request (config) {
            var CSRF_URL = '/csrfToken';//get the csrf token from the sails server
			if(config.url == CSRF_URL || config.method == "GET"){
				return config;
			}
			// sailsjs hasn't time limit for csrf token, so it is safe to cache this
			// remove this to request a new token
			if(_token){ 
                config.headers['X-CSRF-Token'] = _token;
				return config;
			}
 
			var deferred = $q.defer();
			var $http = $injector.get('$http');
			$http.get(CSRF_URL).success(function (response, status, headers) {
                if(response._csrf){
                    _token = response._csrf;
					config.headers['X-CSRF-Token'] = _token;
				}
				deferred.resolve(config);
			}).error(function (response , status , headers) {
				deferred.reject(response);
			});
            return deferred.promise;
	    }		
	}//end of csrfInterceptor

})();