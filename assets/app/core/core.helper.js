(function () {
	'use strict';
	//core.helper.js
	angular.module('core')
		.factory('urlHelper', urlHelper)
		.filter('capitalize', capitalize)
		.filter('jpaginate', jpaginate);

	urlHelper.$inject = ['BASE_URL'];
	function urlHelper (BASE_URL) {
		//create urls for transferring across app routes
		//public
		//applicant
		//agency
		return {
			base: BASE_URL + "/#!/",
			account: BASE_URL + "/account#!/",
		};
	}//end of coreHelper

	function capitalize () {
		return function(input, all) {
		  return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
		}		
	}//end of capitalize filter
	
	jpaginate.$inject = ['$log'];
	function jpaginate ($log) {
	    return function(collection, currentPage, maxSize) {
	    	$log.info(collection, currentPage, maxSize);
	        return collection.slice((currentPage - 1)*maxSize,currentPage * maxSize);
	    }
	}
})();