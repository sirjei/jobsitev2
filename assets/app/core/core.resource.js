(function () {
	'use strict';
	//core.resource.js
	angular.module('core')
		.factory('vesselResource',vesselResource)
		.factory('salaryRangeResource',salaryRangeResource)
		.factory('rankResource',rankResource)
		.factory('countryResource', countryResource)
		.factory('stateResource', stateResource)
		.factory('trainingResource', trainingResource)
		.factory('documentResource', documentResource)
		.factory('jobResource', jobResource)
		.factory('applicationResource', applicationResource)
		.factory('autoSuggestionResource', autoSuggestionResource);

	autoSuggestionResource.$inject = [''];
	function autoSuggestionResource (rankResource) {
		var jobsSuggestionsLoaded = false;


	}

	applicationResource.$inject = ['$resource'];
	function applicationResource ($resource) {
		return {
			applicant: $resource('/api/v1/applicant/applications',{id:'@id'}, {
				sendApplication: {
					method: "POST"
				},
				// sendApplicationWithLogin: {
				// 	url: "/api/v1/applicant/applications",
				// 	method: "POST"
				// }				
			}),
		};
	}//applicationResource

	jobResource.$inject = ['$resource'];
	function jobResource ($resource) {
		return {
			agency: $resource('/api/v1/agency/post',{id:'@id'}, {

			}),
			// search: $resource('/api/v1/search/job',{id:'@id'}, {

			// }),
			post: $resource('/api/v1/jobs',{id:'@id'}, {

			})			
		};

		////////////////

	}//postResource

	function vesselResource () {
		return {
			getList: getList
		};
		///////
		function getList () {
			return [
			"Bulk Carrier",
			"Car Carrier",
			"Oil Tanker",
			"Chemical Tanker",
			"VLCC",
			"LPG Carrier",
			"Container",
			"Dry Cargo",
			"Reefer",
			"Reefer container",
			"RORO",
			"OBO",
			"Multi-Purpose Vessel",
			"Cruise Ship",
			"Offshore Vessel",
			"Coastal Vessel",
			"TUG",
			"Dredger",
			"General Cargo",
			"Product Tanker",
			"Survey Vessel",
			"Wood/Log Carrier",
			"Fishing Vessel",
			"LNG Carrier",
			"Aframax Tanker",
			"Crude Oil Tanker",
			"D P Vessel",
			"FPSO",
			];
		}
	}//end of vesselResource

	function salaryRangeResource () {
		return {
			getList: getList
		};
		///////////

		function getList () {
			return [
				{
					min: 0,
					max: 1000
				},{
					min: 1000,
					max: 2000
				},{
					min: 2000,
					max: 3000
				},{
					min: 3000,
					max: 4000
				},{
					min: 5000,
					max: 6000
				}																
			];
		}
	}

	function rankResource () {

		var ranks = [{
				order: 1,
				category: "Deck Department",
				name: "Master",
			},{
				order: 2,
				category: "Deck Department",
				name: "Chief Officer",
			},{
				order: 3,
				category: "Deck Department",
				name: "2nd Officer",
			},{
				order: 4,
				category: "Deck Department",
				name: "3rd Officer",
			},{
				order: 5,
				category: "Deck Department",
				name: "Radio Officer",
			},{
				order: 6,
				category: "Deck Department",
				name: "Deck Cadet",
			},{
				order: 7,
				category: "Deck Department",
				name: "Trainee Cadet",
			},{
				order: 8,
				category: "Deck Department",
				name: "Bosun",
			},{
				order: 9,
				category: "Deck Department",
				name: "Deck Fitter",
			},{
				order: 10,
				category: "Deck Department",
				name: "AB",
			},{
				order: 11,
				category: "Deck Department",
				name: "OS",
			},{
				order: 12,
				category: "Deck Department",
				name: "GP",
			},{
				order: 13,
				category: "Deck Department",
				name: "Crane Operator",
			},{
				order: 14,
				category: "Deck Department",
				name: "Junior Officer",
			},{
				order: 15,
				category: "Deck Department",
				name: "Junior Officer",
			},
			////// Engineering
			{
				order: 1,
				category: "Engineering Department",
				name: "Chief Engineer",
			},{
				order: 2,
				category: "Engineering Department",
				name: "2nd Engineer",				
			},{
				order: 3,
				category: "Engineering Department",
				name: "3rd Engineer",				
			},{
				order: 4,
				category: "Engineering Department",
				name:"4th Engineer",
			},{
				order: 5,
				category: "Engineering Department",
				name:"5th Engineer",
			},{
				order: 6,
				category: "Engineering Department",
				name:"Electrical Engineer",
			},{
				order: 7,
				category: "Engineering Department",
				name:"Electrical Officer",
			},{
				order: 8,
				category: "Engineering Department",
				name:"Electro Technical Officer",
			},{
				order: 9,
				category: "Engineering Department",
				name:"Junior Engineer",
			},{
				order: 10,
				category: "Engineering Department",
				name:"Asst. Electrical Officer",
			},{
				order: 11,
				category: "Engineering Department",
				name:"Trainee Engineer",
			},{
				order: 12,
				category: "Engineering Department",
				name:"Reefer Engineer",
			},{
				order: 13,
				category: "Engineering Department",
				name:"Reefer Mechanic",
			},{
				order: 14,
				category: "Engineering Department",
				name:"Gas Engineer",
			},{
				order: 15,
				category: "Engineering Department",
				name:"Engine Fitter",
			},{
				order: 16,
				category: "Engineering Department",
				name:"Motorman",
			},{
				order: 17,
				category: "Engineering Department",
				name:"Wiper",
			},{
				order: 18,
				category: "Engineering Department",
				name:"Travel Fitter",
			},{
				order: 19,
				category: "Engineering Department",
				name:"Travel Wiper",
			},{
				order: 20,
				category: "Engineering Department",
				name:"Pielstik Engineer",
			},
			///// steward
			{
				order: 1,
				category: "Steward Department",
				name: "Hotel Manager",
			},{
				order: 1,
				category: "Steward Department",
				name: "Purser",
			},{
				order: 1,
				category: "Steward Department",
				name: "Social Director",
			},{
				order: 1,
				category: "Steward Department",
				name: "Sports Director",
			},{
				order: 1,
				category: "Steward Department",
				name: "Chef",
			},{
				order: 1,
				category: "Steward Department",
				name: "Souse Chef",
			},{
				order: 1,
				category: "Steward Department",
				name: "Chief Cook",
			},{
				order: 1,
				category: "Steward Department",
				name: "2nd Cook",
			},{
				order: 1,
				category: "Steward Department",
				name: "Chief Steward",
			},{
				order: 1,
				category: "Steward Department",
				name: "Steward",
			},{
				order: 1,
				category: "Steward Department",
				name: "Cabin Attendant",
			},{
				order: 1,
				category: "Steward Department",
				name: "Bar Tender",
			},{
				order: 1,
				category: "Steward Department",
				name: "Musician",
			},{
				order: 1,
				category: "Steward Department",
				name: "Laundry Men",
			},{
				order: 1,
				category: "Steward Department",
				name: "Security Guard",
			},{
				order: 1,
				category: "Steward Department",
				name: "Safety Officer",
			}];

		return {
			getList: getList,
			getDeckDepartment: getDeckDepartment,
			getEngineeringDepartment: getEngineeringDepartment,
			getStewardDepartment: getStewardDepartment
		};			
		//////////////
		function getList () {
			return ranks;
		}//end of getRanks

		function getDeckDepartment () {
			var deck = [];
			for (var i = 0; i < ranks.length; i++) {
				if (ranks[i].category === "Deck Department") {
					deck.push(ranks[i].name);
				}
			};
			return deck;
		}

		function getEngineeringDepartment () {
			var engineering = [];
			for (var i = 0; i < ranks.length; i++) {
				if (ranks[i].category === "Engineering Department") {
					engineering.push(ranks[i].name);
				}
			};
			return engineering;			
		}

		function getStewardDepartment () {
			var steward = [];
			for (var i = 0; i < ranks.length; i++) {
				if (ranks[i].category === "Steward Department") {
					steward.push(ranks[i].name);
				}
			};
			return steward;			
		}				
	}//end of rankResource

	function stateResource () {
		return {
			getList: getList
		};

		////////////////
		function getList () {
	    return [
			{code: "AL", name: "Alabama"},
			{code: "AK", name: "Alaska"},
			{code: "AZ",name: "Arizona"},
			{code: "AR", name:"Arkansas"},
			{code: "CA", name:"California"},
			{code: "CO", name:"Colorado"},
			{code: "CT", name:"Connecticut"},
			{code: "DE", name:"Delaware"},
			{code: "DC", name:"District Of Columbia"},
			{code: "FL", name:"Florida"},
			{code: "GA", name:"Georgia"},
			{code: "HI", name:"Hawaii"},
			{code: "ID", name:"Idaho"},
			{code: "IL", name:"Illinois"},
			{code: "IN", name:"Indiana"},
			{code: "IA", name:"Iowa"},
			{code: "KS", name:"Kansas"},
			{code: "KY", name:"Kentucky"},
			{code: "LA", name:"Louisiana"},
			{code: "ME", name:"Maine"},
			{code: "MD", name:"Maryland"},
			{code: "MA", name:"Massachusetts"},
			{code: "MI", name:"Michigan"},
			{code: "MN", name:"Minnesota"},
			{code: "MS", name:"Mississippi"},
			{code: "MO", name:"Missouri"},
			{code: "MT", name:"Montana"},
			{code: "NE", name:"Nebraska"},
			{code: "NV", name:"Nevada"},
			{code: "NH", name:"New Hampshire"},
			{code: "NJ", name:"New Jersey"},
			{code: "NM", name:"New Mexico"},
			{code: "NY", name:"New York"},
			{code: "NC", name:"North Carolina"},
			{code: "ND", name:"North Dakota"},
			{code: "OH", name:"Ohio"},
			{code: "OK", name:"Oklahoma"},
			{code: "OR", name:"Oregon"},
			{code: "PA", name:"Pennsylvania"},
			{code: "RI", name:"Rhode Island"},
			{code: "SC", name:"South Carolina"},
			{code: "SD", name:"South Dakota"},
			{code: "TN", name:"Tennessee"},
			{code: "TX", name:"Texas"},
			{code: "UT", name:"Utah"},
			{code: "VT", name:"Vermont"},
			{code: "VA", name:"Virginia"},
			{code: "WA", name:"Washington"},
			{code: "WV", name:"West Virginia"},
			{code: "WI", name:"Wisconsin"},
			{code: "WY", name:"Wyoming"}];			
		}//end of get list

	}

	function countryResource () {
		return {
			getList: getList,
			getCountryName: getCountryName
		};

		//////
		function getList (){
			return [
			{code: 'ad',name: 'Andorra'},
			{code: 'ae',name: 'United Arab Emirates'},
			{code: 'af',name: 'Afghanistan'},
			{code: 'ag',name: 'Antigua'},
			{code: 'ai',name: 'Anguilla'},
			{code: 'al',name: 'Albania'},
			{code: 'am',name: 'Armenia'},
			{code: 'an',name: 'Netherlands Antilles'},
			{code: 'ao',name: 'Angola'},
			{code: 'ar',name: 'Argentina'},
			{code: 'as',name: 'American Samoa'},
			{code: 'at',name: 'Austria'},
			{code: 'au',name: 'Australia'},
			{code: 'aw',name: 'Aruba'},
			{code: 'ax',name: 'Aland Islands'},
			{code: 'az',name: 'Azerbaijan'},
			{code: 'ba',name: 'Bosnia'},
			{code: 'bb',name: 'Barbados'},
			{code: 'bd',name: 'Bangladesh'},
			{code: 'be',name: 'Belgium'},
			{code: 'bf',name: 'Burkina Faso'},
			{code: 'bg',name: 'Bulgaria'},
			{code: 'bh',name: 'Bahrain'},
			{code: 'bi',name: 'Burundi'},
			{code: 'bj',name: 'Benin'},
			{code: 'bm',name: 'Bermuda'},
			{code: 'bn',name: 'Brunei'},
			{code: 'bo',name: 'Bolivia'},
			{code: 'br',name: 'Brazil'},
			{code: 'bs',name: 'Bahamas'},
			{code: 'bt',name: 'Bhutan'},
			{code: 'bv',name: 'Bouvet Island'},
			{code: 'bw',name: 'Botswana'},
			{code: 'by',name: 'Belarus'},
			{code: 'bz',name: 'Belize'},
			{code: 'ca',name: 'Canada'},
			{code: 'cc',name: 'Cocos Islands'},
			{code: 'cd',name: 'Congo'},
			{code: 'cf',name: 'Central African Republic'},
			{code: 'cg',name: 'Congo Brazzaville'},
			{code: 'ch',name: 'Switzerland'},
			{code: 'ci',name: 'Cote Divoire'},
			{code: 'ck',name: 'Cook Islands'},
			{code: 'cl',name: 'Chile'},
			{code: 'cm',name: 'Cameroon'},
			{code: 'cn',name: 'China'},
			{code: 'co',name: 'Colombia'},
			{code: 'cr',name: 'Costa Rica'},
			{code: 'cs',name: 'Serbia'},
			{code: 'cu',name: 'Cuba'},
			{code: 'cv',name: 'Cape Verde'},
			{code: 'cx',name: 'Christmas Island'},
			{code: 'cy',name: 'Cyprus'},
			{code: 'cz',name: 'Czech Republic'},
			{code: 'de',name: 'Germany'},
			{code: 'dj',name: 'Djibouti'},
			{code: 'dk',name: 'Denmark'},
			{code: 'dm',name: 'Dominica'},
			{code: 'do',name: 'Dominican Republic'},
			{code: 'dz',name: 'Algeria'},
			{code: 'ec',name: 'Ecuador'},
			{code: 'ee',name: 'Estonia'},
			{code: 'eg',name: 'Egypt'},
			{code: 'eh',name: 'Western Sahara'},
			{code: 'er',name: 'Eritrea'},
			{code: 'es',name: 'Spain'},
			{code: 'et',name: 'Ethiopia'},
			{code: 'eu',name: 'European Union'},
			{code: 'fi',name: 'Finland'},
			{code: 'fj',name: 'Fiji'},
			{code: 'fk',name: 'Falkland Islands'},
			{code: 'fm',name: 'Micronesia'},
			{code: 'fo',name: 'Faroe Islands'},
			{code: 'fr',name: 'France'},
			{code: 'ga',name: 'Gabon'},
			{code: 'gb',name: 'United Kingdom'},
			{code: 'gd',name: 'Grenada'},
			{code: 'ge',name: 'Georgia'},
			{code: 'gf',name: 'French Guiana'},
			{code: 'gh',name: 'Ghana'},
			{code: 'gi',name: 'Gibraltar'},
			{code: 'gl',name: 'Greenland'},
			{code: 'gm',name: 'Gambia'},
			{code: 'gn',name: 'Guinea'},
			{code: 'gp',name: 'Guadeloupe'},
			{code: 'gq',name: 'Equatorial Guinea'},
			{code: 'gr',name: 'Greece'},
			{code: 'gs',name: 'Sandwich Islands'},
			{code: 'gt',name: 'Guatemala'},
			{code: 'gu',name: 'Guam'},
			{code: 'gw',name: 'Guinea-bissau'},
			{code: 'gy',name: 'Guyana'},
			{code: 'hk',name: 'Hong Kong'},
			{code: 'hm',name: 'Heard Island'},
			{code: 'hn',name: 'Honduras'},
			{code: 'hr',name: 'Croatia'},
			{code: 'ht',name: 'Haiti'},
			{code: 'hu',name: 'Hungary'},
			{code: 'id',name: 'Indonesia'},
			{code: 'ie',name: 'Ireland'},
			{code: 'il',name: 'Israel'},
			{code: 'in',name: 'India'},
			{code: 'io',name: 'Indian Ocean Territory'},
			{code: 'iq',name: 'Iraq'},
			{code: 'ir',name: 'Iran'},
			{code: 'is',name: 'Iceland'},
			{code: 'it',name: 'Italy'},
			{code: 'jm',name: 'Jamaica'},
			{code: 'jo',name: 'Jordan'},
			{code: 'jp',name: 'Japan'},
			{code: 'ke',name: 'Kenya'},
			{code: 'kg',name: 'Kyrgyzstan'},
			{code: 'kh',name: 'Cambodia'},
			{code: 'ki',name: 'Kiribati'},
			{code: 'km',name: 'Comoros'},
			{code: 'kn',name: 'Saint Kitts And Nevis'},
			{code: 'kp',name: 'North Korea'},
			{code: 'kr',name: 'South Korea'},
			{code: 'kw',name: 'Kuwait'},
			{code: 'ky',name: 'Cayman Islands'},
			{code: 'kz',name: 'Kazakhstan'},
			{code: 'la',name: 'Laos'},
			{code: 'lb',name: 'Lebanon'},
			{code: 'lc',name: 'Saint Lucia'},
			{code: 'li',name: 'Liechtenstein'},
			{code: 'lk',name: 'Sri Lanka'},
			{code: 'lr',name: 'Liberia'},
			{code: 'ls',name: 'Lesotho'},
			{code: 'lt',name: 'Lithuania'},
			{code: 'lu',name: 'Luxembourg'},
			{code: 'lv',name: 'Latvia'},
			{code: 'ly',name: 'Libya'},
			{code: 'ma',name: 'Morocco'},
			{code: 'mc',name: 'Monaco'},
			{code: 'md',name: 'Moldova'},
			{code: 'me',name: 'Montenegro'},
			{code: 'mg',name: 'Madagascar'},
			{code: 'mh',name: 'Marshall Islands'},
			{code: 'mk',name: 'Macedonia'},
			{code: 'ml',name: 'Mali'},
			{code: 'myanmar',name: 'Burma'},
			{code: 'mn',name: 'Mongolia'},
			{code: 'mo',name: 'Macau'},
			{code: 'mp',name: 'Northern Mariana Islands'},
			{code: 'mq',name: 'Martinique'},
			{code: 'mr',name: 'Mauritania'},
			{code: 'ms',name: 'Montserrat'},
			{code: 'mt',name: 'Malta'},
			{code: 'mu',name: 'Mauritius'},
			{code: 'mv',name: 'Maldives'},
			{code: 'mw',name: 'Malawi'},
			{code: 'mx',name: 'Mexico'},
			{code: 'my',name: 'Malaysia'},
			{code: 'mz',name: 'Mozambique'},
			{code: 'na',name: 'Namibia'},
			{code: 'nc',name: 'New Caledonia'},
			{code: 'ne',name: 'Niger'},
			{code: 'nf',name: 'Norfolk Island'},
			{code: 'ng',name: 'Nigeria'},
			{code: 'ni',name: 'Nicaragua'},
			{code: 'nl',name: 'Netherlands'},
			{code: 'no',name: 'Norway'},
			{code: 'np',name: 'Nepal'},
			{code: 'nr',name: 'Nauru'},
			{code: 'nu',name: 'Niue'},
			{code: 'nz',name: 'New Zealand'},
			{code: 'om',name: 'Oman'},
			{code: 'pa',name: 'Panama'},
			{code: 'pe',name: 'Peru'},
			{code: 'pf',name: 'French Polynesia'},
			{code: 'pg',name: 'New Guinea'},
			{code: 'ph',name: 'Philippines'},
			{code: 'pk',name: 'Pakistan'},
			{code: 'pl',name: 'Poland'},
			{code: 'pm',name: 'Saint Pierre'},
			{code: 'pn',name: 'Pitcairn Islands'},
			{code: 'pr',name: 'Puerto Rico'},
			{code: 'ps',name: 'Palestine'},
			{code: 'pt',name: 'Portugal'},
			{code: 'pw',name: 'Palau'},
			{code: 'py',name: 'Paraguay'},
			{code: 'qa',name: 'Qatar'},
			{code: 're',name: 'Reunion'},
			{code: 'ro',name: 'Romania'},
			{code: 'rs',name: 'Serbia'},
			{code: 'ru',name: 'Russia'},
			{code: 'rw',name: 'Rwanda'},
			{code: 'sa',name: 'Saudi Arabia'},
			{code: 'sb',name: 'Solomon Islands'},
			{code: 'sc',name: 'Seychelles'},
			{code: 'sd',name: 'Sudan'},
			{code: 'se',name: 'Sweden'},
			{code: 'sg',name: 'Singapore'},
			{code: 'sh',name: 'Saint Helena'},
			{code: 'si',name: 'Slovenia'},
			{code: 'sj',name: 'Jan Mayen'},
			{code: 'sk',name: 'Slovakia'},
			{code: 'sl',name: 'Sierra Leone'},
			{code: 'sm',name: 'San Marino'},
			{code: 'sn',name: 'Senegal'},
			{code: 'so',name: 'Somalia'},
			{code: 'sr',name: 'Suriname'},
			{code: 'st',name: 'Sao Tome'},
			{code: 'sv',name: 'El Salvador'},
			{code: 'sy',name: 'Syria'},
			{code: 'sz',name: 'Swaziland'},
			{code: 'tc',name: 'Caicos Islands'},
			{code: 'td',name: 'Chad'},
			{code: 'tf',name: 'French Territories'},
			{code: 'tg',name: 'Togo'},
			{code: 'th',name: 'Thailand'},
			{code: 'tj',name: 'Tajikistan'},
			{code: 'tk',name: 'Tokelau'},
			{code: 'tl',name: 'Timorleste'},
			{code: 'tm',name: 'Turkmenistan'},
			{code: 'tn',name: 'Tunisia'},
			{code: 'to',name: 'Tonga'},
			{code: 'tr',name: 'Turkey'},
			{code: 'tt',name: 'Trinidad'},
			{code: 'tv',name: 'Tuvalu'},
			{code: 'tw',name: 'Taiwan'},
			{code: 'tz',name: 'Tanzania'},
			{code: 'ua',name: 'Ukraine'},
			{code: 'ug',name: 'Uganda'},
			{code: 'um',name: 'Us Minor Islands'},
			{code: 'us',name: 'United States'},
			{code: 'uy',name: 'Uruguay'},
			{code: 'uz',name: 'Uzbekistan'},
			{code: 'va',name: 'Vatican City'},
			{code: 'vc',name: 'Saint Vincent'},
			{code: 've',name: 'Venezuela'},
			{code: 'vg',name: 'British Virgin Islands'},
			{code: 'vi',name: 'Us Virgin Islands'},
			{code: 'vn',name: 'Vietnam'},
			{code: 'vu',name: 'Vanuatu'},
			{code: 'wf',name: 'Wallis And Futuna'},
			{code: 'ws',name: 'Samoa'},
			{code: 'ye',name: 'Yemen'},
			{code: 'yt',name: 'Mayotte'},
			{code: 'za',name: 'South Africa'},
			{code: 'zm',name: 'Zambia'},
			{code: 'zw',name: 'Zimbabwe'}];		
		}

		function getCountryName (code) {
			var countries = this.getList(),
				length = countries.length,
				i, country;

			for (i = 0; i < length; i++) {

				if (countries[i].code === code) {
					country =  countries[i].name;
					break;
				}
			};
			return country;
		}
	}

	function trainingResource () {
		return {
			getList: getList
		};

		////////////////

		function getList () {
			return [
				{
					_id:1,
					name: "Basic Safety Course w/ PSSR",
				},{
					_id:2,
					name: "Deck watchkeeping",
				},{
					_id:3,
					name: "Basic Firefigthing",
				},{
					_id:4,
					name: "Engine watchkeeping",
				},{
					_id:5,
					name: "Personal Survival Technique",
				},{
					_id:6,
					name: "Messman Course",
				},{
					_id:7,
					name: "Elementary First Aid",
				},{
					_id:8,
					name: "Catering Mgmt/Culinary Cert. (For Cook/Stwd)",
				},{
					_id:9,
					name: "Personal Safety and Social Responsibility",
				},{
					_id:10,
					name: "PADAMS",
				},{
					_id:11,
					name: "Prof. in Survival Craft and Rescue Boat (PSC)",
				},{
					_id:12,
					name: "MARPOL I",
				},{
					_id:13,
					name: "Advanced Firefighting",
				},{
					_id:14,
					name: "MARPOL II",
				},{
					_id:15,
					name: "Medical Emergency-First Aid (MEFA)",
				},{
					_id:16,
					name: "Ship Security Awareness (SSA)",
				},{
					_id:17,
					name: "Medical Care (MECA)",
				},{
					_id:18,
					name: "Shorebased Fire Fighting (SBFF)",
				},{
					_id:19,
					name: "Automatic Radar Planning (ARPA)",
				},{
					_id:20,
					name: "General Tanker Familiarization (GTF)",
				},{
					_id:21,
					name: "Radar Observer Plotting (ROP)",
				},{
					_id:22,
					name: "Specialized Training on Oil Tankers",
				},{
					_id:23,
					name: "Radar Simulator Course (RSC)",
				},{
					_id:24,
					name: "HAZMAT",
				},{
					_id:25,
					name: "Ship Simulator and Bridge Teamwork (SSBT)",
				},{
					_id:26,
					name: "Dangerous, Hazardous",
				},{
					_id:27,
					name: "Bridge Resource Management (BRM)",
				},{
					_id:28,
					name: "Engine Resource Management (ERM)",
				},{
					_id:29,
					name: "Global Maritime Distress and Safety System (GMDSS)",
				},{
					_id:30,
					name: "Auxiliary Machinery System",
				},{
					_id:31,
					name: "INMARSAT",
				},{
					_id:32,
					name: "Control Engineering",
				},{
					_id:33,
					name: "Ship Restricted Radio Telephone Operators Course (SRROC)",
				},{
					_id:34,
					name: "Hydraulics/Pneumatics",
				},{
					_id:35,
					name: "Shift Security Officer (SSO)",
				},{
					_id:36,
					name: "Smoke Diving",
				},{
					_id:37,
					name: "Electronic Chart Display (ECDIS)",
				},{
					_id:38,
					name: "Crude Oil Washing",
				},{
					_id:39,
					name: "Cargo Handling and Care of Cargo",
				},{
					_id:40,
					name: "Maritime Law for Ships Officers",
				},{
					_id:41,
					name: "Collision Avoidance",
				},{
					_id:42,
					name: "Maritime Refregeration and Air Conditioning",
				}
				];

		}
	}

	function documentResource () {
		return {
			getList: getList
		};

		////////
		function getList () {
			return [
			{
				code: "passport",
				description: "Passport",
				number: null,
				dateIssued: null,
				validUntil: null
			},{
				code: "seamansbook",
				description: "Seaman's Book",
				number: null,
				dateIssued: null,
				validUntil: null
			},{
				code: "usvisa",
				description: "US Visa",
				number: null,
				dateIssued: null,
				validUntil: null
			}
			];
		}
	}
})();