(function () {
	'use strict';
	//core.module.js
	angular.module('core', [
		/*
		* Angular Modules
		*/
		'ngResource',
		'ngSanitize',
		'ngMessages',
		'ngCookies',
		/*
		* Reusable Cross App Module
		*/
		'app.pickadate',
		'app.smoothscroll',
		/*
		* 3rd Party Modules
		*/
		'ui.router',
		'ui.select',
		'ui.bootstrap',
		'ui-notification',
		'truncate',
	]);
})();