(function() {
	'use strict';
	angular.module('core')
		.factory('bookmark', bookmark);

	bookmark.$inject = ['$cookieStore', '$log', '$http', '$rootScope'];
	function bookmark($cookieStore, $log, $http, $rootScope) {
		var bookmarks = loadBookmarks();

		return {
			'addBookmark': addBookmark,
			'bookmarks' : bookmarks,
			'delBookmark' : delBookmark
		};

		//////////////////////////////
		function loadBookmarks() {
			if ($cookieStore.get('bookmarks')) {
				return $cookieStore.get('bookmarks');
			} else {
				return [];
			}
		}

		function addBookmark(job) {
			job = { // minimize job object by saving needed properties only
				'_id': job._id,
				'headline': job.headline,
				'agencyName': job.agencyName
			}

			var found = false

			for (var i = 0; i < bookmarks.length; i++) { // check if job object already exists
				if (bookmarks[i]._id === job._id) {
					found = true;
				}
			};

			if (!found) { // push the job object if not found
				bookmarks.push(job);
				$rootScope.$broadcast("bookmarkAdded", bookmarks);
				$cookieStore.put('bookmarks', bookmarks);
			};

			$log.info($cookieStore.get('bookmarks'));
		}

		function delBookmark(job) {
			var index = bookmarks.indexOf(job);
			bookmarks.splice(index, 1);

			$cookieStore.put('bookmarks', bookmarks);

			$log.info($cookieStore.get('bookmarks'));
		}
	} // end of bookmark
})();