(function () {
	'use strict';
	//app.pickadate.js
	angular.module('app.pickadate', [])
		.directive('pickADate', pickADate)
		.directive('pickATime', pickATime);

	function pickADate(){
		//depends on moment.js
	    var linker = function (scope, elem, attrs, control) {

	    	var $input = angular.element(elem).pickadate({
			    selectYears: 140,
			    //selectMonths: true,
			    editable: false,
			});

			var picker = $input.pickadate('picker');
			picker.on({
			    open: function() {
			    	var ngModel = scope.$eval(attrs.ngModel);
					if (angular.isDefined(ngModel)) {
						picker.set('select',ngModel);
			        }
			    },
			    close: function() {
			        //console.log('Closed now')
			    },
			    render: function() {
			        //console.log('Just rendered anew')
			    },
			    stop: function() {
			        //console.log('See ya')
			    },
			    set: function(thingSet) {
			        //console.log('Set stuff:', thingSet)
			    }
			})

			function validateMin (value,min) {
				var isAfter = moment(value).isAfter(min);
				if (isAfter) {
					control.$setValidity("min", true);
				} else {
					control.$setValidity("min", false);
				}
			}

			function validateMax (value,max) {
				var isBefore = moment(value).isBefore(max);
				if (isBefore) {
					control.$setValidity("max", true);
				} else {
					control.$setValidity("max", false);
				}
			}

			function validateAge (v) {

				
				if (angular.isDefined(attrs.age)) {//if max is set
					var age = scope.$eval(attrs.age);
					var date = moment(v);
					var years = moment().diff(date, 'years');

					if (age < years) {
						control.$setValidity("age", true);
					} else {
						control.$setValidity("age", false);
					}
				}			
			}



			scope.$watch(attrs.ngModel, function(v) {

				if (attrs.ngRequired) {

					if (!control.$error.required) {//if error is not required

						if (angular.isDefined(attrs.max)) {//if max is set
							var max = scope.$eval(attrs.max);
							validateMax(v,max);
						}					
						
						if (angular.isDefined(attrs.min)) {//if max is set
							var min = scope.$eval(attrs.min);
							validateMin(v,min);
						}

						validateAge(v);
					}
				}
			});

			if (angular.isDefined(attrs.max)) {
				
				scope.$watch(attrs.max, function(max) {
					var model = scope.$eval(attrs.ngModel);
					if (attrs.ngRequired) {

						if (!control.$error.required) {//if error is not required
							validateMax(model,max);
						}
					}
				});
			}

			if (angular.isDefined(attrs.min)) {
				
				scope.$watch(attrs.min, function(min) {
					var model = scope.$eval(attrs.ngModel);
					if (attrs.ngRequired) {

						if (!control.$error.required) {//if error is not required
							validateMin(model,min);
						}
					}
				});
			}

		};

	    return {
	        restrict: 'A',
	        require: 'ngModel',
	        link: linker
	    };
	}// pickADate	

	function pickATime (){

	    var linker = function (scope, elem, attrs, control) {

	    	var $input = angular.element(elem).pickatime();
			var picker = $input.pickatime('picker');
			picker.on({
			    open: function() {
			    	var ngModel = scope.$eval(attrs.ngModel);
					if (angular.isDefined(ngModel)) {
						picker.set('select',ngModel);
			        }
			    },
			    close: function() {
			        //console.log('Closed now')
			    },
			    render: function() {
			        //console.log('Just rendered anew')
			    },
			    stop: function() {
			        //console.log('See ya')
			    },
			    set: function(thingSet) {
			        //console.log('Set stuff:', thingSet)
			    }
			})

		};

	    return {
	        restrict: 'A',
	        require: 'ngModel',
	        link: linker
	    };
	}	
})();



