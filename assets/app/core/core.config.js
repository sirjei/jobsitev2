(function () {
	'use strict';
	//core.config.js
	angular.module('core')
		.config(coreConfig)
		.run(coreRun);

	coreConfig.$inject = ['$httpProvider', '$provide', '$locationProvider'];
	function coreConfig ($httpProvider, $provide, $locationProvider) {
		$locationProvider.html5Mode(false).hashPrefix('!');
		

		//boostraps
	    var bootstrappedUser = angular.copy(window.bootstrappedUser);
	    var bootstrappedInstance = angular.copy(window.bootstrappedInstance);
	    var BASE_URL = angular.copy(window.BASE_URL);
	    $provide.constant('CURRENT_USER',bootstrappedUser);
	    $provide.constant('CURRENT_INSTANCE',bootstrappedInstance);	
		$provide.constant('BASE_URL',BASE_URL);

	    //$httpProvider.interceptors.push('errorInterceptor');
	    $httpProvider.interceptors.push('csrfInterceptor');
		
	}//end of coreConfig

	coreRun.$inject = ['$rootScope', '$log', 'authService', 'CURRENT_USER', '$window'];
	function coreRun ($rootScope, $log, authService, CURRENT_USER, $window) {
		//set the current user if page is refreshed
		authService.setBootstrappedUser(CURRENT_USER);


	    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
	        $log.info('$stateChangeError:',toState,error);

	        //Redirection Errors
            if (_.has(error, "action")) {
            	if (error.action == 'redirect') {
	            	if (error.redirect.type == "state") {
	            		$state.go(error.redirect.state);
	            	}
	                
	                if (error.redirect.type == "url") {
	                	$window.location.href = error.redirect.url;
	                }
            	}
			}
	        


	    });//end of state change error		
	}
	
})();