(function () {
	'use strict';
	//login.js
	angular.module('public.login', [])
		.controller('login', login);

	login.$inject = ['$log', '$http', '$window', 'urlHelper', 'authService'];
	function login ($log, $http, $window, urlHelper, authService) {
		var vm = this;

		vm.login = login;
		vm.onChangeEmail = onChangeEmail;
		vm.onChangePass = onChangePass;
		////////////

		function login (form) {

			if (form.$valid) {
				authService.login(vm.input).success(function(response) {
					$log.info("Successfully logged in");
					$window.location.href = urlHelper.account;
				}).error(function(reason,status) {
					$log.error(reason);
					$log.error(status);
					if (status === 400) {
						if(reason.error == "custom") {
							switch (reason.field) {
								case "email":
									form.email.$setValidity('exist',false);
									break;
								case "password":
									form.password.$setValidity('password',false);
									break;	
								case "active":
									form.email.$setValidity('active',false);
									break;																		
							}
						}
					}
				});
			}

		}//end of login

		function onChangeEmail (form) {//reset the unique error 
			form.email.$setValidity('exist',true);
			form.email.$setValidity('active',true);
		}	

		function onChangePass (form) {
			form.password.$setValidity('password',true);				
		}		

	}//end of login


})();