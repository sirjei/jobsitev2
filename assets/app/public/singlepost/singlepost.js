(function () {
	'use strict';

	angular.module('public.singlepost',[])
		.controller('SinglePost', SinglePost)
		.factory('singlePostService', singlePostService);

	SinglePost.$inject = ['$log', 'jobResource', 'postDetails', 'singlePostService', 'Notification'];
	function SinglePost ($log, jobResource, postDetails, singlePostService, Notification) {
		//jobResource
		var vm = this;

		vm.job = postDetails;
		vm.sendApplication = sendApplication;

		//////////

		function sendApplication (job) {
			singlePostService.sendApplication(job, sendApplicationSuccess, sendApplicationError);
		}//end of sendApplication

		function sendApplicationSuccess (response) {
			$log.info(response);
			$log.info("success request again");
			/*
			* If successfull show success notification
			* and show indication that current post have an application
			*/
			Notification.success({message: 'Your application is now being processed', delay: 10000});	
		}

		function sendApplicationError (reason, status) {
			$log.info(reason, status)
			/*
			* If error show error notification
			*/
		}
	}//end of  SinglePost

	singlePostService.$inject = ['applicationResource', '$modal', '$log', 'Notification'];
	function singlePostService (applicationResource, $modal, $log, Notification) {

		return {
			sendApplication: sendApplication
		};
		////

		function sendApplication (job, cbSuccess, cbError) {
			applicationResource.applicant.sendApplication({id: job._id}, {jobId: job._id, agencyId: job.agency._id},
				cbSuccess, 
				function (reason) {
				$log.info(reason);
				switch (reason.status) {
					case 400:
						Notification.info({message: reason.data.message, delay: 10000});	
						break;
					case 401:
						/*
						* if status is 401.. you must log in first..
						* call modal for login
						*/					
						var modalInstance = $modal.open({
					      templateUrl: 'app/layout/modals/login.html',
					      controller: loginModal,
					      controllerAs: 'vm'
					    });
						modalInstance.result.then(loginSuccess(job, cbSuccess, cbError));						
						break;
					case 403:
						/*
						* if status is 403 means the user that is trying to send an application is not an applicant..
						* show notification that the user is not permitted to perform the action
						*/		
						Notification.error({message: 'You are not permitted to perform this action', delay: 10000});						
						break;
					case 404:
						//show 404 page
						$log.info("404");					
						break;												
					case 500:
						//show 500 page
						$log.info("500");				
						break;						
				}

			});			
		}//end of sendApplication

		function loginSuccess (job, cbSuccess, cbError) {
			return function () {
				//if login is successfull.. resend the application
				applicationResource.applicant.sendApplication({id: job._id}, {jobId: job._id, agencyId: job.agency._id}, cbSuccess, cbError);
			}
		}//end of loginSuccess

	}//end of singlePostService

	/*
	* Controller for the login form in Modal 
	*/
	loginModal.$inject = ['$log', 'authService', '$modalInstance'];
	function loginModal ($log, authService, $modalInstance) {
		var vm = this;

		vm.login = login;
		vm.onChangeEmail = onChangeEmail;
		vm.onChangePass = onChangePass;
		////////////

		function login (form) {

			if (form.$valid) {
				authService.login(vm.input).success(function(response) {
					/*
					* if login was successfull
					*	resend the application request and close modal
					*/
					$modalInstance.close();
				}).error(function(reason,status) {
					$log.error(reason);
					$log.error(status);
					if (status === 400) {
						if(reason.error == "custom") {
							switch (reason.field) {
								case "email":
									form.email.$setValidity('exist',false);
									break;
								case "password":
									form.password.$setValidity('password',false);
									break;	
								case "active":
									form.email.$setValidity('active',false);
									break;																		
							}
						}
					}
				});
			}

		}//end of login

		function onChangeEmail (form) {//reset the unique error 
			form.email.$setValidity('exist',true);
			form.email.$setValidity('active',true);
		}	

		function onChangePass (form) {
			form.password.$setValidity('password',true);				
		}	

	}//end of login modal



})();