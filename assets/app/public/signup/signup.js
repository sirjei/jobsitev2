(function () {
	'use strict';
	//signup.js
	angular.module('public.signup')
		.controller('Signup', Signup)
		.controller('SignupApplicant', SignupApplicant)
		.controller('SignupAgency', SignupAgency);

	Signup.$inject = ['$stateParams'];
	function Signup ($stateParams) {
		var vm = this;
		
		vm.formContent = getFormContent($stateParams.form);
		////////////
		function getFormContent(form) {
			switch (form) {
				case "applicant":
					return "app/public/signup/signup-applicant.html";
					break;
				case "agency":
					return "app/public/signup/signup-agency.html";
					break;	
				default:
					return "app/public/signup/signup-applicant.html";
					break;									
			}
		}

	}

	SignupApplicant.$inject = ['$log', 'rankResource', '$http', 'userService', 'urlHelper', '$window'];
	function SignupApplicant ($log, rankResource, $http, userService, urlHelper, $window) {
		var vm = this;

		vm.input = {};
		vm.ranks = rankResource.getList();
		vm.createAccount = createAccount;
		vm.onChange = onChange;
		////////////////
		
		function createAccount (form) {

			if (form.$valid) {
				$http.post('/api/v1/signup/applicant',vm.input).success(function (response) {
					$log.info("MADAFUCKER RESPOND",response);
					userService.setCurrent(response);//cache the user and redirect
					$window.location.href = urlHelper.account;
				}).error(function error (reason, status) {
					$log.info(reason);
					$log.info(status);
					if (status === 400) {
						if (reason.name == "MongoError" && reason.code === 11000) {
							//if email already exists
							form.email.$setValidity('unique',false);
						}

						if (reason.name == "ValidationError" && angular.isDefined(reason.errors.email)) {
							form.email.$setValidity('email',false);
						}
					}	
				});
			}
		}//end of createAccount

		function onChange (form) {//reset the unique error 
			form.email.$setValidity('unique',true);
		}		


	}//end of SignupApplicant

	SignupAgency.$inject = ['$log', '$http', 'countryResource', 'userService'];
	function SignupAgency ($log, $http, countryResource, userService) {
		var vm = this;

		vm.input = {};
		vm.createAccount = createAccount;
		vm.onChange = onChange;
		vm.countries = countryResource.getList();

		//////////////
		function createAccount (form) {

			if (form.$valid) {
				$http.post('/api/v1/signup/agency',vm.input).success(function (response) {
					$log.info(response);
					userService.setCurrent(response);//cache the user and redirect
				}).error(function error (reason, status) {
					$log.info(reason);
					$log.info(status);
					if (status === 400) {
						if (reason.name == "MongoError" && reason.code === 11000) {
							//if email already exists
							form.email.$setValidity('unique',false);
						}

						if (reason.name == "ValidationError" && angular.isDefined(reason.errors.email)) {
							form.email.$setValidity('email',false);
						}
					}	
				});
			}
		}//end of createAccount

		function onChange (form) {//reset the unique error 
			form.email.$setValidity('unique',true);
		}				
	}
	
})();