(function () {
	'use strict';
	//core.config.js
	angular.module('public')
		.config(route);

	route.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider'];
	function route ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
	    //main template
	    $stateProvider
	    	.state('public', homeTemplate())// parent template of common pages
	    	.state('search', searchTemplate())// parent template for search
	    	.state('default', defaultTemplate())
	    //public routes
	    	.state('search.job', searchJob())
	    	.state('default.singlepost', singlePost())
	    	//.state('search.views.singlepost', singlePost())// parent template for search
	    //custom template
	    	.state('login', login())// /login
	    	.state('signup', signup());// /login
	    $urlRouterProvider.otherwise('/');

	    //////////////////////////////////////////////
	    function homeTemplate () {
	    	return {
		        abstract: true,
		        templateUrl: 'app/layout/public/public.html',
	    	};
	    }

	    function defaultTemplate () {
	    	return {
		        abstract: true,
		        templateUrl: 'app/layout/public/public-twosidebars.html',
				controller: 'SearchMain',
				controllerAs: 'vm'
	    	};
	    }	

	    function searchTemplate () {
	    	return {
		        abstract: true,
		        url:"/search",
				templateUrl: 'app/layout/public/public-twosidebars.html',
				controller: 'SearchMain',
				controllerAs: 'vm'
	    	};
	    }

	    function homePage () {
	    	return {
		        url: '/',
		        views: {
		        	'main@public': {
		        		templateUrl: 'app/public/home/home.html',
		        	}
		        }
	    	};
	    }

	    function searchJob () {
	    	return {
		        url: '/job?keywords&position&vesselType&sortBy&country',
		        views: {
		        	'filter@search': {
		        		templateUrl: 'app/public/searchjob/searchjob-filter.html',
		        		controller: 'SearchJobFilter',
		        		controllerAs: 'vm'
		        	},
		        	'main@search': {
		        		templateUrl: 'app/public/searchjob/searchjob-results.html',
		        		controller: 'SearchJobResults',
		        		controllerAs: 'vm'
		        	}
		        }
	    	};
	    }

    

	    function singlePost () {
	    	return {
		        url: '/post?id',
		        views: {
		        	'filter@default': {
		        		templateUrl: 'app/public/singlepost/singlepost-sidenav.html',
		        	},		        	
		        	'main@default': {
		        		templateUrl: 'app/public/singlepost/singlepost.html',
		        		controller: 'SinglePost',
		        		controllerAs: 'vm',
		        		resolve: {
		        			postDetails: ['$q', '$stateParams', 'jobResource', '$log', function ($q, $stateParams, jobResource, $log) {
								var defer = $q.defer();
				                    var id = $stateParams.id;
				                    if (angular.isDefined(id) && id !== "") {
				                    	$log.info("YESSSSS");
				        				jobResource.post.get({id:id}, function (response) {
				        					defer.resolve(response);
				        				}, function (reason) {
				        					defer.reject(reason);
				        				});
				                    } else {
				                    	$log.info("NOOOO");
				                        defer.reject();
				                    }
				    
		        				return defer.promise;
		        			}]
		        		}
		        	}
		        }
	    	};
	    }
	    function login () {
			return {
		        url: '/login',
		        templateUrl: 'app/public/login/login.html',
		        controller: 'login',
		        controllerAs: 'vm',
		        resolve: {
		        	isAuthenticated: ['$q','urlHelper', 'authService', function ($q, urlHelper, authService){
		        		return authService.isAuthenticated($q,urlHelper);
		        	}]
		        }
		    };
	    }

	    function signup () {
			return {
		        url: '/signup?form',
		        templateUrl: 'app/public/signup/signup.html',
		        controller: 'Signup',
		        controllerAs: 'vm'
		    };
	    }	    
	}//end of route()

})();