(function () {
	'use strict';
	// searchmain.module.js
	angular.module('public.search')
		.controller('SearchMain', SearchMain)
		.controller('SearchHeader', SearchHeader);

	SearchMain.$inject = ['$rootScope', '$scope', '$log', '$timeout', '$stateParams'];
	function SearchMain ($rootScope, $scope, $log, $timeout, $stateParams) {
		var vm = this;
		
		// if (_.isEmpty($stateParams)) {
		// 	$log.info("is empty", $stateParams)
		// 	vm.filterSelectionIsOpen = true;
		// 	vm.filterContent = "app/public/searchjob/searchjob-filter-position.html";
		// } else {
			vm.filterSelectionIsOpen = false;
			vm.filterContent = "";			
		//}

		$scope.closeFilterSelection = closeFilterSelection;
		// Broadcast events
		$rootScope.$on('search:toggleFilterSelection', toggleFilterSelection);
		//$rootScope.$on('search:closeFilterSelection', closeFilterSelection);


		//////////
		function toggleFilterSelection (scope, filterSelection) {
			vm.filterSelectionIsOpen = filterSelection.open;
			vm.filterContent = filterSelection.content;
		}

		function closeFilterSelection () {
			vm.filterSelectionIsOpen = false;
		}

		$scope.$watch("keyword", function (v) {
			// $log.info(v);
			// $timeout(function() {
				
			// }, 1000);
		});

	}//end of SearchMain

	SearchHeader.$inject = ['authService', 'userService', 'urlHelper', '$window', '$location'];
	function SearchHeader (authService, userService, urlHelper, $window, $location) {
		var vm = this;

		vm.isLoggedIn = isLoggedIn;
		vm.userName = userName;
		vm.goToDashboard = urlHelper.account;
		vm.logout = logout;
		/////////
		function isLoggedIn () {
			return authService.isLoggedIn();			
		}

		function userName () {
			return userService.getName() || null;
		}

		function logout () {
			authService.logout().success(function(currentUser){
				$window.location.href = $location.absUrl();
			});
		}		
	}//SearchHeader

})();