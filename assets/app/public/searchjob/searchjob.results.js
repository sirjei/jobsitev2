(function () {
	'use strict';

	angular.module('public.searchjob')
		.controller('SearchJobResults',SearchJobResults)
		.directive('scroller',scroller);

	SearchJobResults.$inject = ['$scope', '$log', '$http', '$stateParams', 'bookmark']
	function SearchJobResults ($scope, $log, $http, $stateParams, bookmark) {
		var vm = this, params = $stateParams;
		params.skip = 0;
		vm.results = [];
		vm.getId = getId;
		vm.showMoreItems = showMoreItems;
		$scope.addBookmark = bookmark.addBookmark;

		//populate the results section
		
		$http.get('/api/v1/search/job',{params:params}).success(function (response) {
			params.skip = response.currentSkip + 5;
			vm.results = response.searchResults;
		}).error(function (reason, status) {
			$log.info(reason);
			$log.info(status);
		});

		///////////
		function getId (jobId) {
			alert(jobId);
		}

		function showMoreItems () {
			
			
			if (!vm.loadingMore) {//if still loading wait for it
				vm.loadingMore = true;
				$http.get('/api/v1/search/job',{params:params}).success(function (response) {

					if (response.searchResults.length > 0) {
						vm.results = vm.results.concat(response.searchResults);
						params.skip = response.currentSkip + 5;
						$log.info("show",params.skip);
						//	$scope.$apply();
						vm.loadingMore = false;					
					} else {
						vm.loadingMore = false;	
					}

				}).error(function (reason, status) {
					$log.info(reason);
					$log.info(status);
					vm.loadingMore = false;			
				});					
			}
		

		}
	}//end of SearchJobResults

	scroller.$inject= ['$log']
	function scroller ($log) {
		return {
			link: link,
			restrict: "AE",
			scope: {
				end: "&"
			}
		};

		//////
		function link (scope, elem, attr) {
			var container = elem[0];
            $(elem).on('scroll', function () {
                // $log.info("SCROLLING");

				if (container.offsetHeight + container.scrollTop >= container.scrollHeight) {
					$log.info("END OF SCROLLING");
					scope.end();
				}                
			});
		}
	}

})();