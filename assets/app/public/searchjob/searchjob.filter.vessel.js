(function () {
	'use strict';

	angular.module('public.searchjob')
		.controller('SearchJobFilterVessel',SearchJobFilterVessel);

	SearchJobFilterVessel.$inject = ['$rootScope', '$log', '$stateParams', 'vesselResource'];
	function SearchJobFilterVessel ($rootScope, $log, $stateParams, vesselResource) {
		var vm = this;
		
		vm.vesselTypes = vesselResource.getList();
		vm.selectVessel = selectVessel;
		vm.currentSelected = $stateParams.vesselType;
		/////////////
		function closeFilterSelection () {
			$rootScope.$broadcast('search:toggleFilterSelection', {
				open: false,
				content: ""
			});			
		}		

		function selectVessel (selected) {
			$rootScope.$broadcast('searchjob:selectedVessel', selected);	
		}
	}

})();