(function () {
	'use strict';
	// searchjob.filter.js
	angular.module('public.searchjob')
		.controller('SearchJobFilter', SearchJobFilter)

	SearchJobFilter.$inject = ['$rootScope','$log', '$stateParams', '$state', 'countryResource'];
	function SearchJobFilter ($rootScope, $log, $stateParams, $state, countryResource) {
		var vm = this;//as filter
		
		vm.activeFilter = {
			position: false,
			vesselType: false,
			sortBy: false,
			country: false
		};

		vm.selectedFilters = {
			position: $stateParams.position || "all positions",
			vesselType: $stateParams.vesselType || "all vessels",
			sortBy: $stateParams.sortBy || "highest salary",
			country: $stateParams.country || "worldwide"
		};

		vm.showList = showList;
		vm.getCountryName = getCountryName;
		//events
		$rootScope.$on('searchjob:selectedPosition', selectedPosition);
		$rootScope.$on('searchjob:selectedVessel', selectedVessel);
		$rootScope.$on('searchjob:selectedCountry', selectedCountry);
		$rootScope.$on('searchjob:selectedSort', selectedSort);
		$rootScope.$on('searchjob:clear', clearSelection);
		///////////////
		function showList(selected) {

			var content = "";
			// $log.info(selected);
			// $log.info(vm.activeFilter);
			for (var prop in vm.activeFilter) {

				if (prop !== selected) {
					vm.activeFilter[prop] = false;
				} else {
					vm.activeFilter[prop] = true;
				}
			};

			switch (selected) {
				case "position":
					content = "app/public/searchjob/searchjob-filter-position.html";
					break;
				case "vesselType":
					content = "app/public/searchjob/searchjob-filter-vessel.html";
					break;
				case "sortBy":
					content = "app/public/searchjob/searchjob-filter-sort.html";
					break;
				case "country":
					content = "app/public/searchjob/searchjob-filter-country.html";
					break;	
			}

			//tell searchmain controller to open the filter selection
			$rootScope.$broadcast('search:toggleFilterSelection', {
				open: true,
				content: content
			});
		}

		function selectedPosition (scope, selected) {
			if (selected == "all positions") {
				$state.go('search.job',{position: null});
			} else {
				$state.go('search.job',{position: selected});
			}
			
			close();			
		}

		function selectedVessel (scope, selected) {
			
			if (selected == "all vessels") {
				$state.go('search.job',{vesselType: null});
			} else {
				$state.go('search.job',{vesselType: selected});
			}			
			close();		
		}

		function selectedCountry (scope, selected) {
			
			if (selected == "worldwide") {
				$state.go('search.job',{country: null});
			} else {
				$state.go('search.job',{country: selected});
			}			
			close();
		}	

		function selectedSort (scope, selected) {
			$state.go('search.job',{sortBy: selected});

			close();
		}

		function close () {
			$rootScope.$broadcast('search:toggleFilterSelection', {
				open: false,
				content: ""
			});
		}

		function clearSelection () {
			vm.activeFilter = {
				position: false,
				vesselType: false,
				sortBy: false,
				country: false
			};	
		}

		function getCountryName (code) {
			var countries = countryResource.getList(),
				length = countries.length,
				i, country;

			for (i = 0; i < length; i++) {

				if (countries[i].code === code) {
					country =  countries[i].name;
					break;
				}
			};
			return country;
		}
	}//end of SearchJobFilter
})();