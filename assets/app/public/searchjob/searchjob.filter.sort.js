(function () {
	'use strict';

	angular.module('public.searchjob')
		.controller('SearchJobFilterSort', SearchJobFilterSort);

	SearchJobFilterSort.$inject = ['$rootScope', '$log', '$stateParams'];
	function SearchJobFilterSort ($rootScope, $log, $stateParams) {
		var vm = this;

		vm.selectSort = selectSort;
		vm.currentSelected = $stateParams.sortBy;
		/////////////
		function closeFilterSelection () {
			$rootScope.$broadcast('search:toggleFilterSelection', {
				open: false,
				content: ""
			});		
			$rootScope.$broadcast('searchjob:clear');		
		}		

		function selectSort (selected) {
			$rootScope.$broadcast('searchjob:selectedSort', selected);	
		}
	}

})();