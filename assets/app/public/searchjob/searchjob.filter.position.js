(function () {
	'use strict';

	angular.module('public.searchjob')
		.controller('SearchJobFilterPosition', SearchJobFilterPosition);

	SearchJobFilterPosition.$inject = ['$rootScope', '$log', '$stateParams', 'rankResource'];
	function SearchJobFilterPosition ($rootScope, $log, $stateParams, rankResource) {
		var vm = this;
			
			
		vm.deck = rankResource.getDeckDepartment();
		vm.engineering = rankResource.getEngineeringDepartment();
		vm.steward = rankResource.getStewardDepartment();
		vm.selectPosition = selectPosition;
		vm.currentSelected = $stateParams.position;
		/////////////

		function selectPosition (selected) {
			$rootScope.$broadcast('searchjob:selectedPosition', selected);	
		}
	}

})();