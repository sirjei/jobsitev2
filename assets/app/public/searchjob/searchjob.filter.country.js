(function () {
	'use strict';

	angular.module('public.searchjob')
		.controller('SearchJobFilterCountry', SearchJobFilterCountry);

	SearchJobFilterCountry.$inject = ['$rootScope', '$log', '$stateParams', 'countryResource'];
	function SearchJobFilterCountry ($rootScope, $log, $stateParams, countryResource) {
		var vm = this;

		vm.countries = countryResource.getList();
		vm.selectCountry = selectCountry;
		vm.currentSelected = $stateParams.country;
		/////////////

		function selectCountry (selected) {
			$rootScope.$broadcast('searchjob:selectedCountry', selected);	
		}
	}

})();