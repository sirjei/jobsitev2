(function () {
	'use strict';

	angular.module('public.searchjob')
	.controller("SearchBox", SearchBox);

	SearchBox.$inject = ['$state', '$stateParams', '$scope', '$timeout'];
	function SearchBox ($state, $stateParams, $scope, $timeout) {
		var vm = this;

		vm.search = search;
		vm.keywords = $stateParams.keywords;
		////////
		function search () {
			//alert("TEST");
			$state.go('search.job',{keywords: vm.keywords});
		}
		
		$scope.$watch('vm.keywords', function (v) {
			//$timeout.cancel(true);
			$state.go('search.job',{keywords: v});
			// var timer = $timeout;
			// $timeout.cancel(timer);
			// timer = $timeout(function (){
			// 	$state.go('search.job',{keywords: v});
			// }, 3000, true);
			
		});
	}

})();