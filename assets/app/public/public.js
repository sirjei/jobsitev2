(function () {
	'use strict';

	angular.module('public', [
		'core',
		/*
		* Feature Modules
		*/
		'public.login',
		'public.search',
		'public.signup',
		'public.singlepost'
		/*3rd party modules*/
	]);
})();