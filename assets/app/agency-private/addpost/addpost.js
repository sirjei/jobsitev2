(function () {
	'use strict';
	//addpost.js
	angular.module('agency.post', [])
		.controller("AgencyPostJob",AgencyPostJob);

	AgencyPostJob.$inject = ['$rootScope', '$scope', '$log', 'rankResource', 'salaryRangeResource', 'vesselResource', 'trainingResource', 'jobResource', '$modal', '$state'];
	function AgencyPostJob ($rootScope, $scope, $log, rankResource, salaryRangeResource, vesselResource, trainingResource, jobResource, $modal, $state) {
		var vm = this,
			trainingListCache = trainingResource.getList();

		vm.input = {
			benefits: [],
			qualifications: [],
			trainings: [],
		};

		vm.viewSwitcher = "main";
		vm.salaryRange = salaryRangeResource.getList();

		vm.ranks = rankResource.getList();
		vm.vesselTypes = vesselResource.getList();
		vm.trainingCertificates = [];
		vm.addToList = addToList;
		vm.removeFromList = removeFromList;
		vm.post = post;
		vm.showTrainingSelection = showTrainingSelection;
		vm.applyTrainingList = applyTrainingList;
		////

		function addToList (list, data, form) {
			if (form.$valid) {
				
				if (list.length < 20) {
					form.$setValidity("count", true);
					list.push(data);
				} else {
					form.$setValidity("count", false);
				}
			}
		}

		function removeFromList (list,index, form) {
			list.splice(index,1);
			form.$setValidity("count", true);
		}

		function post (basicInfoForm) {
			
			basicInfoForm.$setSubmitted();//set as submitted to show errors /api/v1/agency/job/post
			if (basicInfoForm.$valid) {

			    var modalInstance = $modal.open({
			      templateUrl: 'app/agency-private/addpost/addpostconfirm.html',
			      controller: postConfirm,
			      controllerAs: 'vm'
			    });

			    modalInstance.result.then(function () {
					jobResource.agency.save({}, vm.input, function (response) {
						$log.info(response);
						$state.go('agency.manageposts');
					},function (reason, status) {
						$log.info(reason);
						$log.info(status);
					})
			    });				

			}

		}



		function showTrainingSelection () {
			vm.viewSwitcher = "training";
			var list = vm.input.trainings;
			vm.trainingCertificates = angular.copy(trainingListCache);
			//loop the list and set the selected items from the input.certificates
			for (var i = 0; i < list.length; i++) {

				for (var x = 0; x < vm.trainingCertificates.length; x++) {
					if (vm.trainingCertificates[x]._id === list[i]._id) {
						vm.trainingCertificates[x].$checked = true;
					}
				};
			};
		}

		function applyTrainingList () {
			//update the vm.input.certificates from the checked items of vm.trainingCertificates
			var certificates = vm.trainingCertificates,
				checkedCertificates = [];

			for (var i = 0; i < certificates.length; i++) {
				if (certificates[i].$checked) {
					checkedCertificates.push(certificates[i]);
				}
			};

			vm.input.trainings = checkedCertificates;
			vm.viewSwitcher = "main";
		}

	}//end

	postConfirm.$inject = ['$modalInstance'];
	function postConfirm ($modalInstance) {
		var vm = this

		vm.ok = ok;
		vm.cancel = cancel;
		///////
		function ok () {
			$modalInstance.close();
		};

		function cancel () {
			$modalInstance.dismiss('cancel');
		};		  			
	}//postConfirm	
})();