(function () {
	'use strict';
	//agency.config.js
	angular.module('agency')
		.config(route);

	route.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider'];
	function route ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
	    //main template

	    $stateProvider
	    	.state('agency', parentTemplate())
		//public routes
	     	.state('agency.dashboard', dashboard())// parent template for search
	     	.state('agency.addpost', addPost())// parent template for search
	     	.state('agency.manageposts', managePosts())// parent template for search
	    // //custom template
	    // 	.state('login', login());// /login
	    $urlRouterProvider.otherwise('/');

	    // //////////////////////////////////////////////
	    function parentTemplate () {
	    	return {
	    		abstract:true,
		        views: {
		        	'header': {
		        		templateUrl: 'app/layout/agency/agency-header.html',
		        		controller: "AgencyHeader",
		        		controllerAs: "vm"
		        	},
		        	'shortcuts': {
		        		templateUrl: 'app/layout/agency/agency-shortcuts.html',
		        		controller: "AgencyShortcuts",
		        		controllerAs: "vm"
		        	}		        	
		        }
		    };
	    }

	    function dashboard () {
	    	return {
	    		url: '/',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/agency-private/dashboard/dashboard.html',
		        	},
		        }
		    };
	    }

	    function addPost () {
	    	return {
	    		url: '/post',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/agency-private/addpost/addpost.html',
		        		controller: "AgencyPostJob",
		        		controllerAs: "vm"
		        	},
		        }
		    };
	    }
	    function managePosts () {
	    	return {
	    		url: '/manageposts',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/agency-private/manageposts/manageposts.html',
		        	},
		        }
		    };
	    }
	}//end of route()

})();