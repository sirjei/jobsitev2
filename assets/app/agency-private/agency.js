(function () {
	'use strict';
	//agency.js
	angular.module('agency')
		.controller("AgencyHeader", AgencyHeader)
		.controller("AgencyShortcuts", AgencyShortcuts);

	AgencyHeader.$inject = ['$log', '$http', '$window', 'urlHelper', 'authService'];
	function AgencyHeader ($log, $http, $window, urlHelper, authService) {
		var vm = this;

		vm.logout = logout;
		vm.homeUrl = urlHelper.base;
		///////////////
		function logout () {
			authService.logout().success(function(currentUser){
				$window.location.href = urlHelper.base;
			});
			
		}
	}	

	AgencyShortcuts.$inject = ['$log', 'userService', 'urlHelper'];
	function AgencyShortcuts ($log, userService, urlHelper) {
		var vm = this;

		vm.currentUser = userService.getName();
		vm.agency = userService.getAgencyName();
	}
})();