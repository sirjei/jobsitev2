(function () {
	'use strict';
	//primarydoc.js
	angular.module('applicant.managecv')
		.controller('EditTraining',EditTraining);

	EditTraining.$inject = ['$log', '$filter', '$state', 'cvResource', 'trainingResource'];
	function EditTraining ($log, $filter, $state, cvResource, trainingResource) {
		var vm = this;
			vm.input = {
				trainings: []
			};

		//populate the fields
		cvResource.get(get);
		vm.trainingList = trainingResource.getList();
		//methods
		vm.update = update;
		////////
		function get(response) {
			
			vm.input.trainings = response.trainings || [];		
			$log.info(response);
			//loop the saved list of the user and compare it with the full training list to display the checked items of the user
			for (var i = 0, inputTrainings = vm.input.trainings, resourceTrainings = vm.trainingList; 
				i < inputTrainings.length; i++) {
				
				for (var x = 0; x < resourceTrainings.length; x++) {

					if (inputTrainings[i]._id == resourceTrainings[x]._id) {
						resourceTrainings[x].$checked = true;
					}

				};

			};		
		}

		function update (form) {
			
			//loop and get all the checked items
			for (var i = 0, list = vm.trainingList,newList = []; i < list.length; i++) {
				if (list[i].$checked) {
					newList.push(list[i]);
				}
			};

			vm.input.trainings = newList;
			
			cvResource.updateTraining({}, vm.input, function (response) {
				$state.go('applicant.cvsummary');
			}, function (reason) {
				$log.info(reason);
			});
		}		
	}
})();