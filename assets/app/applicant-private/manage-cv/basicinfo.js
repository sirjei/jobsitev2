(function () {
	'use strict';
	//basicinfo.js
	angular.module('applicant.managecv')
		.controller('EditBasicInfo',EditBasicInfo);

	EditBasicInfo.$inject = ['$log', '$filter', '$state', 'cvResource', 'countryResource', 'stateResource', 'rankResource', 'salaryRangeResource'];
	function EditBasicInfo ($log, $filter, $state, cvResource, countryResource, stateResource, rankResource, salaryRangeResource) {
		var vm = this;
			vm.input = {};

		//populate the fields
		cvResource.get(get);
	
		//methods
		vm.ranks = rankResource.getList();
		$log.info(vm.ranks);
		vm.gender = ["male", "female"];
		vm.countries = countryResource.getList();
		vm.states = stateResource.getList();
		vm.salaryRange = salaryRangeResource.getList();
		vm.update = update;
		////////
		function get(response) {
			
			vm.input = {
				rank: $filter('capitalize')(response.rank) || null,
				name: $filter('capitalize')(response.user.name) || "",
				gender: response.gender || "",
				nationality: response.nationality || "",
				contactOne: response.contactOne || "",
				contactTwo: response.contactTwo || "",
				expectedSalaryRange: response.expectedSalaryRange || null,
				address: response.address || "",
				city: response.city || "",
				state: response.state || "",
				country: response.country || "", 
				zipCode: response.zipCode || "", 
			};				
		}

		function update (form) {
			
			if (form.$valid) {
				cvResource.updateBasicInfo({}, vm.input, function (response) {
					$state.go('applicant.cvsummary');
				}, function (reason) {
					$log.info(reason);
				});
			} else {
				alert("INVALID");
			}
		}		
	}
})();