(function () {
	'use strict';
	//managecv.js
	angular.module('applicant.managecv',[])
		.controller('CVSummary',CVSummary)
		.directive('cvJumpLinks',cvJumpLinks);
		//.controller('DeleteJobHistory',DeleteJobHistory);

	CVSummary.$inject = ['$log', 'cvResource', 'countryResource', '$modal', 'cvDetails', '$anchorScroll', '$location'];
	function CVSummary ($log, cvResource, countryResource, $modal, cvDetails, $anchorScroll, $location) {
		var vm = this;

		//load every sections
		//cvResource.get(get);
		vm.details = cvDetails;
		vm.getCountryName = getCountryName;
		vm.destroy = destroy;
		
		////////

	   vm.gotoAnchor = function(element) {
	      // set the location.hash to the id of
	      // the element you wish to scroll to.
	      $location.hash(element);

	      // call $anchorScroll()
	      $anchorScroll();
	    };

		function getCountryName (code) {
			return countryResource.getCountryName(code);
		}

		function destroy (row) {//delete for history

		    var modalInstance = $modal.open({
		      templateUrl: 'delete.html',
		      controller: DeleteJobHistory,
		      controllerAs: 'vm'
		    });

		    modalInstance.result.then(function (selectedItem) {
		      	cvResource.destroyJobHistory({id:row._id},deleteSuccess(row),deleteError);
		    });	

		}

		DeleteJobHistory.$inject = ['$modalInstance'];
		function DeleteJobHistory ($modalInstance) {
			var vm = this

			vm.ok = ok;
			vm.cancel = cancel;
			///////
			function ok () {
				$modalInstance.close();
			};

			function cancel () {
				$modalInstance.dismiss('cancel');
			};		  			
		}//DeleteJobHistory	

		function deleteSuccess (row) {
			return function (response) {
				$log.info(response,row);
				var index = vm.details.history.indexOf(row);
				vm.details.history.splice(index,1);
			};
		}

		function deleteError (reason) {
			$log.info(reason);
		}
	}//CVSummary

	function cvJumpLinks () {
		return {
			linker: linker,
		}

		////////
		function linker (scope, elem, attr) {

		}
	}


})();