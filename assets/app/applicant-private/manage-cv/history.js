(function () {
	'use strict';
	//history.js
	angular.module('applicant.managecv')
		.controller('EditJobHistory',EditJobHistory)
		.controller('AddJobHistory',AddJobHistory);

	EditJobHistory.$inject = ['$log', '$filter', '$state', 'cvResource', 'history', 'rankResource'];
	function EditJobHistory ($log, $filter, $state, cvResource, history, rankResource) {
		var vm = this;

		vm.ranks = rankResource.getList();
		vm.input = {
			position: history.position,
			agency: history.agency,
			signOn: $filter('date')(history.signOn,'d MMMM, yyyy'),
			signOff: $filter('date')(history.signOff,'d MMMM, yyyy'),
			vesselType: history.vesselType,
			vesselName: history.vesselName,
			engineType: history.engineType,
			grtkw: history.grtkw,
			masterNationality: history.masterNationality,
			principal: history.principal,
			flag: history.flag,
			route: history.route,
			salary: {value:history.salary.value}
		};

		
		//load every sections
		vm.update = update;


		////////
		function update (form) {
			if (form.$valid) {
				cvResource.updateJobHistory({id:history._id},vm.input, addSuccess, addError);
			}
		}

		function addSuccess (response) {
			$log.info(response);
			$state.go('applicant.cvsummary');
		}

		function addError (reason) {
			$log.info(reason);
		}		
	}

	AddJobHistory.$inject = ['$log', '$state', 'rankResource', 'cvResource'];
	function AddJobHistory ($log, $state, rankResource, cvResource) {
		var vm = this;
		
		vm.ranks = rankResource.getList();
		vm.input = {
			salary: {value:0}
		};

		
		vm.add = add;
		///////////

		function add (form) {
			if (form.$valid) {
				cvResource.addJobHistory({},vm.input, addSuccess, addError);
			}
		}

		function addSuccess (response) {
			$log.info(response);
			$state.go('applicant.cvsummary');
		}

		function addError (reason) {
			$log.info(reason);
		}
	}

})();