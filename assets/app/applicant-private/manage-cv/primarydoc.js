(function () {
	'use strict';
	//primarydoc.js
	angular.module('applicant.managecv')
		.controller('EditPrimaryDoc',EditPrimaryDoc);

	EditPrimaryDoc.$inject = ['$log', '$filter', '$state', 'userPrimaryDocs', 'primaryDocs', 'cvResource'];
	function EditPrimaryDoc ($log, $filter, $state, userPrimaryDocs, primaryDocs, cvResource) {
		var vm = this;

		vm.pickadateOptions = { format: 'yyyy-mm-dd' };
		vm.documents = primaryDocs;
		init();

		//methods
		vm.update = update;
		vm.open = open;
		vm.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};
		////////


		function update (doc,form) {
			
			if (form.$valid) {
				cvResource.updateOnePrimaryDoc({code:doc.code},doc, function (response) {
					//$state.go('applicant.cvsummary');
					alert("Success");
				}, function (reason) {
					$log.info(reason);
				});
			} else {
				alert("INVALID");
			}
		}	



	  	function init () {
			for (var i = 0; i < vm.documents.length; i++) {
				
				for (var x = 0; x < userPrimaryDocs.length; x++) {
					if (vm.documents[i].code == userPrimaryDocs[x].code) {
						vm.documents[i].number = userPrimaryDocs[x].number;
						vm.documents[i].dateIssued = $filter('date')(userPrimaryDocs[x].dateIssued,'d MMMM, yyyy');
						vm.documents[i].validUntil = $filter('date')(userPrimaryDocs[x].validUntil,'d MMMM, yyyy');
					}
				};
			};	  		
	  	}

	}
})();