(function () {
	'use strict';
	//cvresource.js
	angular.module('applicant.cvResource',[])
		.factory('cvResource',cvResource);

	cvResource.$inject = ['$resource'];
	function cvResource ($resource) {
		return $resource("/api/v1/applicant/cv",
			{
				id:'@id',
				code: '@code'
			},
			{
				updateBasicInfo: {
					url: '/api/v1/applicant/cv/basicinfo',
					method: 'PUT',
				},
				updateOnePrimaryDoc: {
					url: '/api/v1/applicant/cv/primarydoc',
					method: 'PUT',
				},
				updateTraining: {
					url: '/api/v1/applicant/cv/trainings',
					method: 'PUT',
				},	
				addJobHistory: {
					url: '/api/v1/applicant/cv/jobhistory',
					method: 'POST',
				},
				getJobHistory: {
					url: '/api/v1/applicant/cv/jobhistory',
					method: 'GET',
				},					
				updateJobHistory: {
					url: '/api/v1/applicant/cv/jobhistory',
					method: 'PUT',
				},
				destroyJobHistory: {
					url: '/api/v1/applicant/cv/jobhistory',
					method: 'DELETE',
				},															
			});

	}

})();