(function () {
	'use strict';

	angular.module('applicant', [
		'core',

		/*
		* Feature Modules
		*/
		'applicant.managecv',
		'applicant.cvResource'
	]);
})();