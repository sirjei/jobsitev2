(function () {
	'use strict';
	//applicant.config.js
	angular.module('applicant')
		.config(route);

	route.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider'];
	function route ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
	    //main template

	    $stateProvider
	    	.state('applicant', parentTemplate())
		//public routes
	     	.state('applicant.cvsummary', cvsummary())
	     	.state('applicant.editcv', basicInfo())
	     	.state('applicant.primarydoc', primaryDoc())
	     	.state('applicant.traininglist', trainingList())
	     	.state('applicant.edithistory', editHistory())
	     	.state('applicant.addhistory', addHistory())
	    // //custom template
	    // 	.state('login', login());// /login
	    $urlRouterProvider.otherwise('/');

	    // //////////////////////////////////////////////
	    function parentTemplate () {
	    	return {
	    		abstract:true,
		        views: {
		        	'header': {
		        		templateUrl: 'app/layout/applicant/applicant-header.html',
		        		controller: "ApplicantHeader",
		        		controllerAs: "vm"
		        	},
		        	'shortcuts': {
		        		templateUrl: 'app/layout/applicant/applicant-shortcuts.html',
		        		controller: "ApplicantShortcuts",
		        		controllerAs: "vm"
		        	}		        	
		        }
		    };
	    }

	    function cvsummary () {
	    	return {
	    		url: '/',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/applicant-private/manage-cv/cvsummary.html',
		        		controller: 'CVSummary',
		        		controllerAs: 'vm',
		        		resolve: {
		        			primaryDocs: ['$q', 'documentResource', function ($q, documentResource) {
		        				var defer = $q.defer();

		        				defer.resolve(documentResource.getList());

		        				return defer.promise;
		        			}],
		        			cvDetails: ['$q', 'cvResource', function ($q, cvResource) {
		        				var defer = $q.defer();

		        				cvResource.get(function (response) {
		        					defer.resolve(response);
		        				},function (reason) {
		        					defer.reject(reason);
		        				})
		        				
								return defer.promise;
		        			}],
		        		}		        		
		        	},
		        }
		    };
	    }

	    function basicInfo () {
	    	return {
	    		url: '/basicinfo',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/applicant-private/manage-cv/basicinfo.html',
		        		controller: 'EditBasicInfo',
		        		controllerAs: 'vm'
		        	},
		        }
		    };	    	
	    }

	    function primaryDoc () {
	    	return {
	    		url: '/primarydoc',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/applicant-private/manage-cv/primarydoc.html',
		        		controller: 'EditPrimaryDoc',
		        		controllerAs: 'vm',
		        		resolve: {
		        			primaryDocs: ['$q', 'documentResource', function ($q, documentResource) {
		        				var defer = $q.defer();

		        				defer.resolve(documentResource.getList());

		        				return defer.promise;
		        			}],
		        			userPrimaryDocs: ['$q', 'cvResource', function ($q, cvResource) {
		        				var defer = $q.defer();

		        				cvResource.get(function (response) {
		        					defer.resolve(response.documents);
		        				},function (reason) {
		        					defer.reject(reason);
		        				})
		        				
								return defer.promise;
		        			}],
		        		}
		        	},
		        }
		    };	    	
	    }

	    function trainingList () {
	    	return {
	    		url: '/training',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/applicant-private/manage-cv/training.html',
		        		controller: 'EditTraining',
		        		controllerAs: 'vm'
		        	},
		        }
		    };	    	
	    }

	    function editHistory () {
	    	return {
	    		url: '/jobhistory/edit?id',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/applicant-private/manage-cv/history-edit.html',
		        		controller: 'EditJobHistory',
		        		controllerAs: 'vm',
		        		resolve: {
				            history: ["$q", "$stateParams", "cvResource", function($q, $stateParams, cvResource){
				                var defer = $q.defer();
				                    var id = $stateParams.id;
				                    if (angular.isDefined(id) && id !== "") {
				                        cvResource.getJobHistory({id:id}, function(history){
				                            defer.resolve(history);    
				                        }, function(reason){
				                            defer.reject(reason);
				                        });
				                    } else {
				                        defer.reject();
				                    }
				                return defer.promise;                  
				            }]    
		        		}
		        	},
		        }
		    };	    	
	    }	

	    function addHistory () {
	    	return {
	    		url: '/jobhistory/add',
	    		views: {
		        	'main@': {
		        		templateUrl: 'app/applicant-private/manage-cv/history-add.html',
		        		controller: 'AddJobHistory',
		        		controllerAs: 'vm'
		        	},
		        }
		    };	    	
	    }	        	    	    
	}//end of route()

})();