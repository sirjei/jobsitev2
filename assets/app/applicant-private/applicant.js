(function () {
	'use strict';

	angular.module('applicant')
		.controller("ApplicantHeader", ApplicantHeader)
		.controller("ApplicantShortcuts", ApplicantShortcuts);

	ApplicantHeader.$inject = ['$log', '$http', '$window', 'urlHelper', 'authService'];
	function ApplicantHeader ($log, $http, $window, urlHelper, authService) {
		var vm = this;

		vm.logout = logout;
		vm.homeUrl = urlHelper.base;
		///////////////
		function logout () {
			authService.logout().success(function(currentUser){
				$window.location.href = urlHelper.base;
			});
			
		}
	}//end of ApplicantHeader

	ApplicantShortcuts.$inject = ['$log', 'userService', 'urlHelper'];
	function ApplicantShortcuts ($log, userService, urlHelper) {
		var vm = this;

		vm.currentUser = userService.getName();
		vm.searchJobUrl = urlHelper.base + "search/job";
	}	

})();