(function() {
	'use strict';
	angular.module('public.searchjob')
		.controller('BookmarkList', BookmarkList);

	BookmarkList.$inject = ['$scope', '$log', 'bookmark', '$rootScope'];
	function BookmarkList($scope, $log, bookmark, $rootScope) {
		$scope.bookmarks = bookmark.bookmarks;
		$scope.delBookmark = bookmark.delBookmark;
		$rootScope.$on("bookmarkAdded", function(scope, data) { // updates bookmarks on adding
			$scope.bookmarks = data;
		});
	}
})();