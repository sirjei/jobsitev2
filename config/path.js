/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



    
module.exports.path = {
    baseUrl: function (req) {
        var url = req.protocol + "://" + req.host + ((sails.config.port) ? ":"+sails.config.port: "");
        
        return url;
    },
    root: __dirname+'/../',//outside config
    views: __dirname+'/../views',
    public: {
        root: __dirname+'/../.tmp/public',
        uploads: __dirname+'/../.tmp/public/uploads',
        uploadPhotoDir: function (id, filename) {
            if (filename) {
                return __dirname+'/../.tmp/public/uploads/'+ id + '/photo/'+filename;
            }else{
                return __dirname+'/../.tmp/public/uploads/'+ id + '/photo';
            }            
        },
        uploadDocDir: function (id, filename) {
            if (filename) {
                return __dirname+'/../.tmp/public/uploads/'+ id + '/doc/'+filename;
            }else{
                return __dirname+'/../.tmp/public/uploads/'+ id + '/doc';
            }            
        }
    },
    temp: {
        root: __dirname+'/../.tmp/',
        uploads: __dirname+'/../.tmp/uploads',
    },
    assets: {
        root: __dirname+'/../assets',
        uploads: __dirname+'/../assets/uploads',
        uploadPhotoDir: function (id, filename) {
            if (filename) {
                return __dirname+'/../assets/uploads/'+ id + '/photo/'+filename;
            }else{
                return __dirname+'/../assets/uploads/'+ id + '/photo';
            }
        },
        uploadDocDir: function (id, filename) {
            if (filename) {
                return __dirname+'/../assets/uploads/'+ id + '/doc/'+filename;
            }else{
                return __dirname+'/../assets/uploads/'+ id + '/doc';
            }            
        }        
    },
};