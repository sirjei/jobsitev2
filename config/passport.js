var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy;
	

passport.serializeUser(function(user, done) {
    var session = {
      type: user.accountType,
      _id: user._id
    };

    done(null, session);
});

passport.deserializeUser(function(session, done) {
  if (session.type == 'administrator') {
    //if account is admin
    db.Administrator.findOne({_id: session._id},'_id accountType').exec(function(err,user){
       done(err, user);
    });
  } else {
    //if account is not admin
    
    switch (session.type) {
      case 'applicant':
        db.User.findOne({_id: session._id},'accountType name applicant').populate('applicant', 'photo').exec(function(err,user){
           done(err, user);
        });
        break;
      case 'agency':
        db.User.findOne({_id: session._id},'accountType name agency').populate('agency', 'name photo map').exec(function(err,user){
           done(err, user);
        });      
        break;        
    }
   
    
  }

});

passport.use('default',new LocalStrategy(
	{
    usernameField: 'email',
    passwordField: 'password'
  },
	function(email, password, done){
		
		db.User.findOne({email:email}).exec(function(err,user){
			if(err){ 
                return done(err,null); 
			}else{
				if(user){

            if (user.active) {//check if this is an active account

                  if(user.validatePass(password)){
                      return done(null,user);
                  }else{
                      return done(null,false, {error: 'custom', field:'password', message : 'Password is incorrect'});
                  }    

            } else {
              return done(null,false, {error: 'custom', field:'active', message : 'Account is currently inactive'});
            }

				}else{
					return done(null,false,{error: 'custom', field:'email', message : 'Email does not exist'});
				}				
			}

		});
	}
));

passport.use('admin',new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password'
    },
  function(email, password, done){
    
    db.Administrator.findOne({email:email}).exec(function(err,user){
      if(err){ 
                return done(err,null); 
      }else{
        if(user){

            if (user.active) {//check if this is an active account

              if (user.validated){//if account is already activated

                  if(user.validatePass(password)){
                      return done(null,user);
                  }else{
                      return done(null,false, {error: 'custom', field:'password', message : 'Password is incorrect'});
                  }    

              }else{
                return done(null,false, {error: 'custom', field:'validated', message : 'Account not yet validated',email: user.email});
              }

            } else {
              return done(null,false, {error: 'custom', field:'active', message : 'Account is currently inactive'});
            }

        }else{
          return done(null,false,{error: 'custom', field:'email', message : 'Email does not exist'});
        }
      }
    });
  }
));

module.exports = {
 http: {
    customMiddleware: function(app){
      //console.log('express midleware for passport');
      app.use(passport.initialize());
      app.use(passport.session());
    }
  }
};

