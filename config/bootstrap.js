/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  	var selectedDBConfig = sails.config.models.connection;
  	var connection = sails.config.connections[selectedDBConfig];
  	//sails.log.info('model config',connection);
  	
	var mongoose = require('mongoose');
	//mongoose.connect('mongodb://'+connection.host+'/'+connection.database);
	 mongoose.connect('mongodb://'+connection.user+':'+connection.password+'@ds055680.mongolab.com:'+connection.port+'/'+connection.database);
	/**
	 
	* We check if the connection is ok
	 
	* If so we will continue to load everything ...
	 
	*/
	 
	var db = mongoose.connection;
	sails.log.info('Try to connect to MongoDB via Mongoose ...');
	db.on('error', function (error) {
		//console.error.bind(console, 'Mongoose connection error:');
		sails.log.warn('Error !',error);
		cb(error);
	});
	db.once('open', function callback() {
		sails.log.info('Connected to MongoDB !');
	  	cb();
	});  
};
