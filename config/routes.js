/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  

  '/': 'main',
  '/account': 'main.account',
  
  //'GET /app/private/*.tpl.html': 'main.privateTemplates',
  //'GET /app/public/**/*.html': 'main.publicTemplates',
  //'GET /**/*.js': 'main.scripts',
  'POST /api/v1/ensure/email': 'user.ensureEmail',
  'POST /api/v1/ensure/agencyName': 'user.ensureAgencyName',

  'POST /api/v1/invite/admin': 'signup.admin',

  'POST /api/v1/login': 'user.login',
  'POST /api/v1/logout': 'user.logout',
  'POST /api/v1/admin/login': 'admin.login',

  //agency
  
  'GET /api/v1/agency/job/post': 'job.getAllByAgency',
  'GET /api/v1/agency/job/post/:id': 'job.getOneByAgency',
  'PUT /api/v1/agency/job/post/:id': 'job.updateOne',
  'DELETE /api/v1/agency/job/post/:id': 'job.destroyOne',

  'GET /api/v1/public/post': 'job.getAllPublic',
  // 'GET /api/v1/post/:id': 'job.getOne',
  // 'PUT /api/v1/post/:id': 'job.updateOne',
  // 'DELETE /api/v1/post': 'job.destroyAll',
  // 'DELETE /api/v1/post/:id': 'job.destroyOne',

  'GET /api/v1/user/info': 'user.info',
  'POST /api/v1/user/photo': 'uploads.photo',
  'PUT /api/v1/agency/profile': 'agency.updateProfile',
  'PUT /api/v1/agency/map': 'agency.updateMap',
  'PUT /api/v1/applicant/profile': 'applicant.updateProfile',
  'GET /api/v1/agency/social': 'agency.getSocial',
  'PUT /api/v1/agency/social': 'agency.updateSocial',

  //follow & rate
  'PUT /api/v1/applicant/follow/:id': 'applicant.follow',
  'PUT /api/v1/applicant/rate/:id': 'applicant.rate',

  //dashboard
  'GET /api/v1/agency/dashboard/stats': 'dashboard.agencyStats',
  'GET /api/v1/applicant/dashboard/stats': 'dashboard.applicantStats',

  //applications
  
  'GET /api/v1/applicant/applications': 'application.getAllByApplicant',
  'DELETE /api/v1/applicant/applications/:id': 'application.remove',
  'GET /api/v1/agency/applications': 'application.getAllByAgency',
  'POST /api/v1/agency/applications': 'application.deny',
  'GET /api/v1/agency/applications/:id': 'application.getOne',  

  //interview
  'GET /api/v1/applicant/interview': 'interview.getAllByApplicant',
  'DELETE /api/v1/applicant/interview/:id': 'interview.remove',
  'POST /api/v1/agency/interview': 'interview.set',
  'GET /api/v1/agency/interview': 'interview.getAllByAgency',
  'GET /api/v1/agency/interview/:id': 'interview.getOne',
  'PUT /api/v1/agency/interview/:id': 'interview.resched',
  'DELETE /api/v1/agency/interview/:id': 'interview.cancel',  

  //documents
  'GET /api/v1/applicant/documents/primary': 'applicant.getPrimaryDocs',
  'PUT /api/v1/applicant/documents/primary': 'applicant.updatePrimaryDocs',
  'POST /api/v1/applicant/documents/uploads': 'uploads.receive', 
  'GET /api/v1/applicant/documents/uploads': 'uploads.getAll',
  'PUT /api/v1/applicant/documents/uploads/:id': 'uploads.updateOne',
  'DELETE /api/v1/applicant/documents/uploads/:id': 'uploads.deleteOne', 
  'GET /api/v1/agency/documents': 'agency.getDocs',
  'PUT /api/v1/agency/documents': 'agency.updateDocs',       

  //history
  'POST /api/v1/applicant/history': 'history.create',
  'GET /api/v1/applicant/history': 'history.getAll',
  'GET /api/v1/applicant/history/:id': 'history.getOne',
  'PUT /api/v1/applicant/history/:id': 'history.updateOne',
  'DELETE /api/v1/applicant/history/:id': 'history.destroy',


  //admin api

  //training
  'POST /api/v1/admin/settings/training': 'settings.createTrainingList',
  'GET /api/v1/admin/settings/training': 'settings.getTrainingList',
  'POST /api/v1/admin/settings/training/:id': 'settings.updateTrainingList',
  'DELETE /api/v1/admin/settings/training/:id': 'settings.destroyTrainingList',
  //documents
  'POST /api/v1/admin/settings/document': 'settings.createDocumentList',
  'GET /api/v1/admin/settings/document': 'settings.getDocumentList',
  'POST /api/v1/admin/settings/document/:id': 'settings.updateDocumentList',
  'DELETE /api/v1/admin/settings/document/:id': 'settings.destroyDocumentList', 
  //settings
  'POST /api/v1/admin/settings/job': 'settings.createJobList',
  'GET /api/v1/admin/settings/job': 'settings.getJobList',
  'POST /api/v1/admin/settings/job/:id': 'settings.updateJobList',
  'DELETE /api/v1/admin/settings/job/:id': 'settings.destroyJobList',   

  'POST /api/v1/admin/settings/currency': 'settings.createCurrencyList',
  'GET /api/v1/admin/settings/currency': 'settings.getCurrencyList',
  'POST /api/v1/admin/settings/currency/:id': 'settings.updateCurrencyList',
  'DELETE /api/v1/admin/settings/currency/:id': 'settings.destroyCurrencyList',    

  'POST /api/v1/admin/settings/vessel': 'settings.createVesselList',
  'GET /api/v1/admin/settings/vessel': 'settings.getVesselList',
  'POST /api/v1/admin/settings/vessel/:id': 'settings.updateVesselList',
  'DELETE /api/v1/admin/settings/vessel/:id': 'settings.destroyVesselList',  
  
  'POST /api/v1/admin/settings/faq': 'faqs.create',
  'GET /api/v1/admin/settings/faq': 'faqs.getAll',
  'GET /api/v1/admin/settings/faq/:id': 'faqs.getOne',
  'POST /api/v1/admin/settings/faq/:id': 'faqs.update',
  'DELETE /api/v1/admin/settings/faq/:id': 'faqs.destroy',    
  // 'GET /api/v1/job': '',//find jobs
  // 'GET /api/v1/job/:id': '',//findOne job
  // 'GET /api/v1/job/:id/': '',//findOne job

  // 'POST /api/v1/job/:id': '',//create job (agency only)
  // 'PUT /api/v1/job/:id': '',//update job (agency only)
  // 'DELETE /api/v1/job/:id': '',//delete job (agency only)
  
  //'*': 'main.otherwise',
  
  'POST /geo': 'user.geo',

  //////////new revision
  'POST /api/v1/signup/applicant': 'signup.applicant',
  'POST /api/v1/signup/agency': 'signup.agency',  
  
  //applications
  'POST /api/v1/applicant/applications?:id': 'application.create',

  'GET /api/v1/applicant/cv': 'applicant.getFullCVDetails',
  'PUT /api/v1/applicant/cv/basicinfo': 'applicant.basicInfo',
  'POST /api/v1/applicant/cv/primarydoc': 'applicant.primaryDocs',
  'PUT /api/v1/applicant/cv/primarydoc?:code': 'applicant.updateOnePrimaryDoc',
  'PUT /api/v1/applicant/cv/trainings': 'applicant.trainings',

  'POST /api/v1/applicant/cv/jobhistory': 'history.create',
  'GET /api/v1/applicant/cv/jobhistory?:id': 'history.getOne',
  'PUT /api/v1/applicant/cv/jobhistory?:id': 'history.updateOne',
  'DELETE /api/v1/applicant/cv/jobhistory?:id': 'history.destroy',  

  'GET /api/v1/jobs': 'job.getOne',
  'GET /api/v1/search/job': 'search.jobs',

  'POST /api/v1/agency/post': 'job.create',

  //job bookmarks
  'POST /api/v1/bookmark/job': 'bookmark.addJobs',
  'GET /api/v1/bookmark/job': 'bookmark.getAllJobs',
  'PUT /api/v1/bookmark/job': 'bookmark.addOneJob',
  'DELETE /api/v1/bookmark/job?:id': 'bookmark.removeOneJob',

  'GET /api/v1/autosuggestions/job': 'job.getAutoSuggestions',
  'GET /api/v1/createkeywords': 'job.createKeywords',
};
