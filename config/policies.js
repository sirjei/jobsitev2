/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions (`true` allows public     *
  * access)                                                                  *
  *                                                                          *
  ***************************************************************************/

  '*': true,

  /***************************************************************************
  *                                                                          *
  * Here's an example of mapping some policies to run before a controller    *
  * and its actions                                                          *
  *                                                                          *
  ***************************************************************************/
	// RabbitController: {

		// Apply the `false` policy as the default for all of RabbitController's actions
		// (`false` prevents all access, which ensures that nothing bad happens to our rabbits)
		// '*': false,

		// For the action `nurture`, apply the 'isRabbitMother' policy
		// (this overrides `false` above)
		// nurture	: 'isRabbitMother',

		// Apply the `isNiceToAnimals` AND `hasRabbitFood` policies
		// before letting any users feed our rabbits
		// feed : ['isNiceToAnimals', 'hasRabbitFood']
	// }
      main: {
        '*': true,
        //privateTemplates: ['sessionAuth']
      },
      user: {
        '*': true,
        info: ['sessionAuth'],
      },      
      dashboard: {
        '*': true,
        agencyStats: ['sessionAuth','isAgency'],
        applicantStats: ['sessionAuth','isApplicant'],
      },
      job: {
        '*': true,
        create: ['sessionAuth','isAgency'],
        getAllByAgency: ['sessionAuth','isAgency'],
        getOneByAgency: ['sessionAuth','isAgency'],
        updateOne: ['sessionAuth','isAgency'],
        destroyOne: ['sessionAuth','isAgency'],
      },
      application: {
        '*': true,
        create: ['sessionAuth','isApplicant'],
        getAllByAgency: ['sessionAuth','isAgency'],
        getAllByApplicant: ['sessionAuth','isApplicant'],
        deny: ['sessionAuth','isAgency'],
        remove: ['sessionAuth','isApplicant'],
      },
      interview: {
        '*': true,
        set: ['sessionAuth','isAgency'], 
        getAllByAgency: ['sessionAuth','isAgency'],
        getAllByApplicant: ['sessionAuth','isApplicant'],    
        cancel: ['sessionAuth','isAgency'],
        remove: ['sessionAuth','isApplicant'],            
        resched: ['sessionAuth','isAgency'],
        getOne: ['sessionAuth','isAgency'], 
      },
      agency: {
        '*': true,
        updateProfile: ['sessionAuth','isAgency'],
        updateMap: ['sessionAuth','isAgency'],
        getSocial: ['sessionAuth','isAgency'],
        updateSocial: ['sessionAuth','isAgency'],
        getDocs: ['sessionAuth','isAgency'],
        updateDocs: ['sessionAuth','isAgency'],
      },
      applicant: {
        '*': true,
        updateProfile: ['sessionAuth','isApplicant'],
        getPrimaryDocs: ['sessionAuth','isApplicant'],
        updatePrimaryDocs: ['sessionAuth','isApplicant'], 
        follow: ['sessionAuth','isApplicant'],  
      },
      uploads: {
        '*': true,
        receive: ['sessionAuth','isApplicant', 'isDocumentDirExist'],   
        updateOne: ['sessionAuth','isApplicant', 'isDocumentDirExist'],   
        deleteOne: ['sessionAuth','isApplicant', 'isDocumentDirExist'],   
        getAll: ['sessionAuth','isApplicant'],
        photo: ['sessionAuth','isPhotoDirExist']
      },
      history: {
        '*': true,
        create: ['sessionAuth','isApplicant'],
        updateOne: ['sessionAuth','isApplicant'],
        destroy: ['sessionAuth','isApplicant'],
        getOne: ['sessionAuth','isApplicant'],
        getAll: ['sessionAuth','isApplicant'],
      }
};
